/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bengkel;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Windows-8.1
 */
public class LoginPageController implements Initializable {
   
    DBConnection db;
    GUIStyle gui;
    
    @FXML
    private Button lpLogin;
    @FXML
    private Button lpSginup;
    @FXML
    private Button lpCancel;
    @FXML
    private TextField lpUserFie;
    @FXML
    private PasswordField lpPassFie;
    @FXML
    private Hyperlink forgetPass;
    @FXML
    private Label warning;


    public LoginPageController() throws SQLException, ClassNotFoundException {
        db = new DBConnection();
        gui = new GUIStyle();
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       warning.setVisible(false);
    }    
    
    public void cancelListener() {
        gui.clearStage();
    }
   
    public void loginListener() throws Exception {
        String verify;
        if (lpUserFie.getText().isEmpty() || lpPassFie.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please Fill In Username Or Password", "Warning", JOptionPane.ERROR_MESSAGE);
        } else {
            verify = db.login(lpUserFie.getText(), lpPassFie.getText());
            if (verify.equals("user")) {
                gui.clearStage();
                gui.chgScene("PersonalInfo.fxml");
            } else if (verify.equals("admin")) {
                gui.clearStage();
                gui.chgScene("AdminMenu.fxml");
            } else {
                warning.setVisible(true);
                lpUserFie.clear();
                lpPassFie.clear();
            }

        }

    }
    
    public void signupListener() throws Exception {
        gui.clearStage();
        gui.addStage("SignUp.fxml");
    }
    
    public void forgetPassListener() throws Exception {
        gui.clearStage();
        gui.addStage("ForgetPassword.fxml");
    }
    
    
}
