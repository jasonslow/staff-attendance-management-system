/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bengkel;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Windows-8.1
 */
public class User {
 
        private StringProperty Name = null;
        private StringProperty Date = null;
        private StringProperty TimeIn = null;
        private StringProperty TimeOut = null;
        private StringProperty OT = null;
        private StringProperty Late = null;
        private StringProperty Type = null;
        private StringProperty Desc = null;
        private BooleanProperty Admin = null;
        private BooleanProperty Check = null;
        private IntegerProperty TDate = null;
        private IntegerProperty TLate = null;
        private IntegerProperty TOt = null;
        private IntegerProperty TLeave = null;
        


        
        public User(String Name, String timeIn, String timeOut) {
            this.Name = new SimpleStringProperty(Name);
            this.TimeIn = new SimpleStringProperty(timeIn);
            this.TimeOut = new SimpleStringProperty(timeOut);  
        }
        
        public User(String Name, int TDate, int TLate, int TOt, int TLeave) {
            this.Name = new SimpleStringProperty(Name);
            this.TDate = new SimpleIntegerProperty(TDate);
            this.TLate = new SimpleIntegerProperty(TLate);
            this.TOt = new SimpleIntegerProperty(TOt);
            this.TLeave = new SimpleIntegerProperty(TLeave);
        }
        
        public User(String Date, String timeIn, String timeOut, String OT, String Late) {
            this.TimeIn = new SimpleStringProperty(timeIn);
            this.TimeOut = new SimpleStringProperty(timeOut);  
            this.Date = new SimpleStringProperty(Date);  
            this.OT = new SimpleStringProperty(OT);
            this.Late = new SimpleStringProperty(Late);
        }
        
        public User(String Name) {
            this.Name = new SimpleStringProperty(Name);
        }
        
        public User(boolean Check, String Name, LocalDate plDate, String Type, String Desc) {
            this.Name = new SimpleStringProperty(Name);
            this.Date = new SimpleStringProperty(plDate.toString());
            this.Type = new SimpleStringProperty(Type);
            this.Desc = new SimpleStringProperty(Desc);
            this.Check = new SimpleBooleanProperty(Check);
        }
        
        public User(String Name, boolean Admin) {
            this.Name = new SimpleStringProperty(Name);
            this.Admin = new SimpleBooleanProperty(Admin);
        }
        
        public String getName() {
            return Name.get();
        }
        
        public String getTimeIn() {
            return TimeIn.get();
        }
        
        public String getTimeOut() {
            return TimeOut.get();
        }
        
        public String getType() {
            return Type.get();
        }
        
        public String getDate() {
            return Date.get();
        }
        
        public String getDesc() {
            return Desc.get();
        }
        
        public String getOT() {
            return OT.get();
        }
        
        public String getLate() {
            return Late.get();
        }
        
        public boolean getCheck() {
            return Check.get();
        }
        
        public boolean getAdmin() {
            return Admin.get();
        }
        
        public int getTDate() {
            return TDate.get();
        }
        
        public int getTLate() {
            return TLate.get();
        }
        
        public int getTOt() {
            return TOt.get();
        }
        
        public int getTLeave() {
            return TLeave.get();
        }
        
        public StringProperty nameProperty() {
            return Name;
        }
        
        public StringProperty timeInProperty() {
            return TimeIn;
        }
        
        public StringProperty timeOutProperty() {
            return TimeOut;
        }
        
        public StringProperty OTProperty() {
            return OT;
        }
        
        public StringProperty LateProperty() {
            return Late;
        }
        
        public StringProperty dateProperty() {
            return Date;
        }
        
        public StringProperty typeProperty() {
            return Type;
        }
        
        public StringProperty descProperty() {
            return Desc;
        }
        
        public BooleanProperty adminProperty() {
            return Admin;
        }
        
         public BooleanProperty checkProperty() {
            return Check;
         }
        
}
