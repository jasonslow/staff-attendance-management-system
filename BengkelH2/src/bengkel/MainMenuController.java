/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bengkel;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.util.Duration;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Windows-8.1
 */
public class MainMenuController implements Initializable {
   
    GUIStyle gui;
    
    /**
     * Initializes the controller class.
     */
    @FXML
    private Button mmAttend;
    
    @FXML
    private Button mmLeaApp;
    
    @FXML
    private Button mmLogin;
    
    @FXML
    private Label mmTime;
    
    @FXML
    private Button about;
    
    
    public MainMenuController() {
        
        gui = new GUIStyle();
        bindToTime();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    /**
     *
     * @throws Exception
     */
    public void mmAttendButtonListener() throws Exception {
        String fxml = "AttendanceList.fxml";
        gui.chgScene(fxml);
    }
    
    public void mmLoginButtonListener() throws Exception {
       gui.addStage("LoginPage.fxml");
    }
    
    public void mmLeaAppButtonListener() throws Exception {
       String fxml = "LeaveAppMenu.fxml";
       gui.chgScene(fxml);
    }
    
    public void mmAboutListener() throws Exception {
       JOptionPane.showMessageDialog(null, "       Attendance System\n                 Version\n                      1.0\nDeveloped by Low Sheng Loong\n     lsloong1012@gmail.com\n");
    }
    
    private void bindToTime() {
        Timeline timeline;
        timeline = new Timeline(
                new KeyFrame(Duration.seconds(0), (ActionEvent actionEvent) -> {
                    Calendar time = Calendar.getInstance();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss aa");
                    mmTime.setText(simpleDateFormat.format(time.getTime()));
                }),
                new KeyFrame(Duration.seconds(1))
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

            
     
}
