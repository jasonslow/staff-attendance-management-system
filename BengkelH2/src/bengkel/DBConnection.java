/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bengkel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author Windows-8.1
 */
public class DBConnection {

    private Connection c;
    private final String url = "jdbc:h2:~/db/bengkel";
    private final String user = "root";
    private final String pass = "toor";
    private static String UID;
    private static String verify;
    private static ObservableList<User> saved;
    private ObservableList<User> leaved;
    private File file;
    private Scanner input;
    private int start, end;

    public DBConnection() throws SQLException, ClassNotFoundException {
        Class.forName("org.h2.Driver");
        c = DriverManager.getConnection(url, user, pass);

    }

    public String test() throws SQLException {
        Statement stm = c.createStatement();
        String sql = "SELECT UID FROM USER";
        ResultSet rs = stm.executeQuery(sql);
        rs.next();
        String ans = rs.getString(1);
        stm.close();
        c.close();
        return ans;
    }

    public void createtbl() throws SQLException {
        Statement stm = c.createStatement();
        String sql = "CREATE TABLE USER (UID VARCHAR(6) NOT NULL ,U_IMG VARCHAR(15) NOT NULL,U_NAME VARCHAR(99) NOT NULL,U_IC VARCHAR(99) UNIQUE NOT NULL,U_GEN CHAR(1) NOT NULL,U_DOB DATE NOT NULL,U_NATION VARCHAR(13) NOT NULL,U_RACE VARCHAR(99) NOT NULL,U_RELI VARCHAR(99) NOT NULL,U_HP INT NOT NULL,U_HOME INT NOT NULL,U_EMAIL VARCHAR(99) NOT NULL,U_FADD VARCHAR(99) NOT NULL,U_FPOST INT NOT NULL,U_FCITY VARCHAR(99) NOT NULL,U_FSTA VARCHAR(15) NOT NULL,U_SADD VARCHAR(99) DEFAULT NULL,U_SPOST INT DEFAULT NULL,U_SCITY VARCHAR(99) DEFAULT NULL,U_SSTA VARCHAR(15) DEFAULT NULL,PRIMARY KEY (UID))";
        stm.execute(sql);
        sql = "CREATE TABLE USER_ACC (UID VARCHAR(6) NOT NULL,USERNAME VARCHAR(99) NOT NULL,PASSWORD VARCHAR(99) NOT NULL,VER_ADMIN CHAR(1) DEFAULT 'N',SEC_ANS VARCHAR(15) NOT NULL,PRIMARY KEY (USERNAME, UID),CONSTRAINT uid_fk1 FOREIGN KEY (UID) REFERENCES USER(UID))";
        stm.execute(sql);
        sql = "CREATE TABLE ATTENDANCE (UID VARCHAR(6) NOT NULL,WORK_DATE DATE NOT NULL,TIME_IN TIME DEFAULT NULL,TIME_OUT TIME DEFAULT NULL,PRIMARY KEY (WORK_DATE, UID),CONSTRAINT uid_fk2 FOREIGN KEY (UID) REFERENCES USER(UID))";
        stm.execute(sql);
        sql = "CREATE TABLE APPLYLEAVE (L_DATE DATE NOT NULL,L_TYPE VARCHAR(30) DEFAULT NULL,L_DESC VARCHAR(99) DEFAULT NULL,UID VARCHAR(6) NOT NULL,PRIMARY KEY (L_DATE, UID),CONSTRAINT uid_fk3 FOREIGN KEY (UID) REFERENCES USER(UID))";
        stm.execute(sql);
        stm.close();
    }

    public String login(String username, String password) throws SQLException {
        Connection c1 = DriverManager.getConnection(url, user, pass);
        String sql = "SELECT UID,VER_ADMIN from USER_ACC WHERE USERNAME = ? AND PASSWORD = ?";
        PreparedStatement stm = c1.prepareStatement(sql);
        stm.setString(1, username);
        stm.setString(2, password);
        ResultSet rs = stm.executeQuery();
        rs.next();
        try {
            UID = rs.getString(1);
            if (rs.getString(2).equals("N")) {
                verify = "user";
            } else {
                verify = "admin";
            }
        } catch (Exception e) {
            verify = "wrong";
        }
        stm.close();
        c1.close();
        return verify;
    }

    public String[] personalInfo() throws SQLException {
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = "SELECT * from USER WHERE UID = '" + UID + "'";
        ResultSet rs = stm.executeQuery(sql);
        ResultSetMetaData rsmd = rs.getMetaData();
        int num = rsmd.getColumnCount();
        String[] pi = new String[num];
        rs.next();
        for (int x = 1; x <= num; x++) {
            pi[(x - 1)] = rs.getString(x);
        }
        stm.close();
        c1.close();
        return pi;
    }

    public ObservableList<User> attendanceName() throws SQLException {
        ObservableList<User> data = FXCollections.observableArrayList();
        String sql;
        try {
            saved.isEmpty();
            data = saved;
        } catch (Exception ex) {
            Statement stm = c.createStatement();
            Date date = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
            String dated = ft.format(date);
            try {
                sql = "SELECT U_NAME FROM USER WHERE UID NOT IN (SELECT UID FROM ATTENDANCE WHERE WORK_DATE = '" + dated + "')";
                ResultSet rs = stm.executeQuery(sql);
                while (rs.next()) {
                    data.add(new User(rs.getString(1), "   -   ", "   -   "));
                }
            } catch (Exception e) {
            }
            try {
                sql = "SELECT u.U_NAME, a.TIME_IN, a.TIME_OUT from USER u JOIN ATTENDANCE a WHERE u.UID = a.UID AND a.WORK_DATE = '" + dated + "'";
                ResultSet rs = stm.executeQuery(sql);
                while (rs.next()) {
                    data.add(new User(rs.getString(1), rs.getString(2), rs.getString(3)));
                }
            } catch (Exception e) {
            }
            stm.close();
            c.close();
        }
        return data;
    }

    public ObservableList<String> name(String type) throws SQLException {
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = null;
        if (type.equals("leave")) {
            sql = "SELECT U_NAME from USER";
        } else {
            sql = "SELECT U_NAME from USER WHERE UID <> '" + UID + "'";
        }
        ResultSet rs = stm.executeQuery(sql);
        ObservableList<String> data = FXCollections.observableArrayList();
        while (rs.next()) {
            data.add(rs.getString(1));
        }
        stm.close();
        c1.close();
        return data;
    }

    public int checkLeave(String date, String name) throws SQLException {
        Connection c1;
        c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = "SELECT COUNT(L_DATE) from APPLYLEAVE WHERE L_DATE = '" + date + "' AND UID = (SELECT UID FROM USER WHERE U_NAME = '" + name + "')";
        ResultSet rs = stm.executeQuery(sql);
        rs.next();
        int time = rs.getInt(1);
        stm.close();
        c1.close();
        return time;
    }

    public void setAttend(ObservableList<User> saved) {
        this.saved = saved;
    }

    public ObservableList<String> workdate(String type) throws SQLException {
        ObservableList<String> date = FXCollections.observableArrayList();
        DateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
        Connection c1;
        c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql;
        if (type.equals("PI")) {
            sql = "SELECT WORK_DATE from ATTENDANCE WHERE UID = '" + UID + "' ORDER BY WORK_DATE ASC";
        } else {
            LocalDate yearmon = LocalDate.now();
            sql = "SELECT WORK_DATE from ATTENDANCE WHERE WORK_DATE NOT LIKE '" + yearmon.toString().substring(0, 7) + "%' ORDER BY WORK_DATE ASC";
        }
        ResultSet rs = stm.executeQuery(sql);
        rs.next();
        date.add(dateFormat.format(rs.getDate(1)));
        String test = dateFormat.format(rs.getDate(1));
        while (rs.next()) {
            if (!test.equalsIgnoreCase(dateFormat.format(rs.getDate(1)))) {
                date.add(dateFormat.format(rs.getDate(1)));
                test = dateFormat.format(rs.getDate(1));
            }
        }
        stm.close();
        c1.close();
        return date;
    }

    public ObservableList<Integer> workyear() throws SQLException {
        ObservableList<Integer> date = FXCollections.observableArrayList();
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        LocalDate year = LocalDate.now();
        String sql = "SELECT MIN(WORK_DATE), MAX(WORK_DATE) from ATTENDANCE WHERE WORK_DATE NOT LIKE '" + year.getYear() + "%'";
        ResultSet rs = stm.executeQuery(sql);
        try {
            rs.next();
            date.add(Integer.parseInt(rs.getString(1).substring(0, 4)));
            int smallyear = Integer.parseInt(rs.getString(1).substring(0, 4));
            int count = Integer.parseInt(rs.getString(2).substring(0, 4)) - Integer.parseInt(rs.getString(1).substring(0, 4));
            for (int x = 1; x <= count; x++) {
                date.add(++smallyear);
            }
        } catch (Exception e) {
        }
        stm.close();
        c1.close();
        return date;
    }

    public ObservableList<User> piLeave() throws SQLException {
        ObservableList<User> data = FXCollections.observableArrayList();
        Connection c1;
        c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = "SELECT L_DATE, L_TYPE, L_DESC from APPLYLEAVE WHERE UID = '" + UID + "'";
        ResultSet rs = stm.executeQuery(sql);
        while (rs.next()) {
            data.add(new User(rs.getString(1), rs.getString(2), rs.getString(3)));
        }
        stm.close();
        c1.close();
        return data;
    }

    public ObservableList<User> piReport(String select) throws SQLException, FileNotFoundException {
        file = new File(System.getProperty("user.home") + "/db/system.txt");
        input = new Scanner(file);
        start = input.nextInt();
        end = input.nextInt();
        ObservableList<User> data = FXCollections.observableArrayList();
        String selected = select.substring(3, 7) + "-" + select.substring(0, 2);
        Connection c1;
        c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = "SELECT WORK_DATE, TIME_IN, TIME_OUT from ATTENDANCE WHERE UID = '" + UID + "' AND WORK_DATE LIKE '" + selected + "%'";
        ResultSet rs = stm.executeQuery(sql);
        while (rs.next()) {
            String out = rs.getString(3);
            String yn = "YES";
            boolean check = Integer.parseInt(rs.getString(2).substring(0, 2)) > start - 1;
            if (out != null) {
                if (check) {
                    if (Integer.parseInt(rs.getString(3).substring(0, 2)) > end) {
                        data.add(new User(rs.getString(1), rs.getString(2), rs.getString(3), String.valueOf(Integer.parseInt(rs.getString(3).substring(0, 2)) - end), yn));
                    } else {
                        data.add(new User(rs.getString(1), rs.getString(2), rs.getString(3), String.valueOf(0), yn));
                    }
                } else {
                    if (Integer.parseInt(rs.getString(3).substring(0, 2)) > end) {
                        data.add(new User(rs.getString(1), rs.getString(2), rs.getString(3), String.valueOf(Integer.parseInt(rs.getString(3).substring(0, 2)) - end), ""));
                    } else {
                        data.add(new User(rs.getString(1), rs.getString(2), rs.getString(3), String.valueOf(0), ""));
                    }
                }
            } else {
                if (check) {
                    data.add(new User(rs.getString(1), rs.getString(2), rs.getString(3), String.valueOf(0), yn));
                } else {
                    data.add(new User(rs.getString(1), rs.getString(2), rs.getString(3), String.valueOf(0), ""));
                }
            }
        }
        stm.close();
        c1.close();
        return data;
    }

    public ObservableList<User> checkAdmin() throws SQLException {
        ObservableList<User> check = FXCollections.observableArrayList();
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = "SELECT u.U_NAME, a.VER_ADMIN from USER u, USER_ACC a WHERE a.UID = u.UID AND a.UID IN (SELECT UID from USER_ACC WHERE UID <> '" + UID + "' )";
        ResultSet rs = stm.executeQuery(sql);
        while (rs.next()) {
            boolean verify;
            if (rs.getString(2).equals("N")) {
                verify = false;
            } else {
                verify = true;
            }
            check.add(new User(rs.getString(1), verify));
        }
        stm.close();
        c1.close();
        return check;
    }

    public boolean checkAtt(String name, String password) throws SQLException {
        boolean ver = false;
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = "SELECT COUNT(u.U_NAME) FROM USER_ACC a NATURAL JOIN USER u WHERE u.U_NAME = '" + name + "' AND a.PASSWORD = '" + password + "'";
        ResultSet rs = stm.executeQuery(sql);
        rs.next();
        if (rs.getInt(1) == 1) {
            ver = true;
        }
        stm.close();
        c1.close();
        return ver;
    }

    public String getVerify() {
        return verify;
    }

    public ObservableList<User> getPending() throws FileNotFoundException {
        leaved = FXCollections.observableArrayList();
        File file = new File(System.getProperty("user.home") + "/db/leave.txt");
        Scanner input = new Scanner(file);
        List<String> data = FXCollections.observableArrayList();
        if (input.hasNext()) {
            while (input.hasNext()) {
                data.add(input.nextLine());
            }
            for (int x = 0; x < data.size(); x = x + 4) {
                LocalDate date = LocalDate.parse(data.get(x + 1));
                if (data.get(x + 3).equals("NULL")) {
                    leaved.add(new User(false, data.get(x), date, data.get(x + 2), ""));
                } else {
                    leaved.add(new User(false, data.get(x), date, data.get(x + 2), data.get(x + 3)));
                }
            }
        } else {
            leaved = null;
        }
        return leaved;
    }

    public void claerLeaved() {
        leaved = null;
    }

    public String getPass() throws SQLException {
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = "SELECT PASSWORD from USER_ACC WHERE UID = '" + UID + "'";
        ResultSet rs = stm.executeQuery(sql);
        rs.next();
        return rs.getString(1);
    }

    public boolean checkUsername(String username) throws SQLException {
        boolean test;
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = "SELECT * from USER_ACC WHERE USERNAME = '" + username + "'";
        ResultSet rs = stm.executeQuery(sql);
        rs.next();
        try {
            rs.getString(1);
            stm.close();
            c1.close();
            test = false;
        } catch (Exception e) {
            stm.close();
            c1.close();
            test = true;
        }
        return test;
    }

    public boolean CheckNameIC(String name, String ic) throws SQLException {
        boolean found = false;
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = null;
        if (name != null) {
            sql = "SELECT UID from USER WHERE U_NAME = '" + name + "' AND U_IC = '" + ic + "'";
        } else {
            sql = "SELECT UID from USER WHERE U_IC = '" + ic + "' AND UID <> '" + UID + "'";
        }
        ResultSet rs = stm.executeQuery(sql);
        rs.next();
        try {
            UID = rs.getString(1);
            stm.close();
            c1.close();
            found = false;
        } catch (Exception e) {
            stm.close();
            c1.close();
            found = true;
        }
        return found;
    }

    public boolean CheckAns(String ans) throws SQLException {
        boolean found = false;
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = "SELECT UID from USER_ACC WHERE SEC_ANS = '" + ans + "' AND UID = '" + UID + "'";
        ResultSet rs = stm.executeQuery(sql);
        rs.next();
        try {
            UID = rs.getString(1);
            stm.close();
            c1.close();
            found = false;
        } catch (Exception e) {
            stm.close();
            c1.close();
            found = true;
        }
        return found;
    }

    public void addStaff(String path, String username, String password, String ans, String name, String ic, String gen, String dob, String nat, String race, String reli, String hp, String home,
            String email, String fadd, String fpost, String fcity, String fsta, String sadd, String spost, String scity, String ssta) throws SQLException, IOException {
        Statement stm = c.createStatement();
        String sql = "SELECT MAX(UID) from USER";
        ResultSet rs = stm.executeQuery(sql);
        rs.next();
        String aUID = "S" + String.valueOf(Integer.parseInt(rs.getString(1).substring(1)) + 1);
        if (!path.contains("\\db\\default.png")) {
            int dot = path.lastIndexOf(".");
            Path source = Paths.get(path);
            Path destination = Paths.get(System.getProperty("user.home")+ "/db/" +aUID + path.substring(dot));
            path = aUID + path.substring(dot);
            Files.copy(source, destination, StandardCopyOption.REPLACE_EXISTING);
        }
        else {
            File files = new File (path);
            path = files.getName();
        }
        if (sadd == null) {
            sql = "INSERT INTO USER VALUES ('" + aUID + "','" + path + "','" + name + "','" + ic + "','" + gen + "','" + dob + "','" + nat + "','"
                    + race + "','" + reli + "','" + hp + "','" + home + "','" + email + "','" + fadd + "','" + fpost + "','" + fcity + "','" + fsta + "','NULL','0','NULL','NULL')";
        } else {
            sql = "INSERT INTO USER VALUES ('" + aUID + "','" + path + "','" + name + "','" + ic + "','" + gen + "','" + dob + "','" + nat + "','"
                    + race + "','" + reli + "','" + hp + "','" + home + "','" + email + "','" + fadd + "','" + fpost + "','" + fcity + "','"
                    + fsta + "','" + sadd + "','" + spost + "','" + scity + "','" + ssta + "')";
        }
        stm.execute(sql);
        sql = "INSERT INTO USER_ACC VALUES ('" + aUID + "','" + username + "','" + password + "','N','" + ans + "')";
        stm.execute(sql);
        stm.close();
        c.close();
    }

    public void addsStaff(String path, String username, String password, String ans, String name, String ic, String gen, String dob, String nat, String race, String reli, String hp, String home,
            String email, String fadd, String fpost, String fcity, String fsta, String sadd, String spost, String scity, String ssta) throws SQLException, IOException {
        Statement stm = c.createStatement();
        String sql = null;
        URL url = this.getClass().getClassLoader().getResource("default.png");
        FileOutputStream output = new FileOutputStream(System.getProperty("user.home") + "/db/default.png");
        InputStream input = url.openStream();
        byte[] buffer = new byte[4096];
        int bytesRead = input.read(buffer);
        while (bytesRead != -1) {
            output.write(buffer, 0, bytesRead);
            bytesRead = input.read(buffer);
        }
        output.close();
        input.close();
        if (!path.equalsIgnoreCase("default.png")) {
            int dot = path.lastIndexOf(".");
            Path source = Paths.get(path);
            Path destination = Paths.get(System.getProperty("user.home") + "/db/S10001" + path.substring(dot));
            path = "S10001" + path.substring(dot);
            Files.copy(source, destination, StandardCopyOption.REPLACE_EXISTING);
        }
        if (sadd == null) {
            sql = "INSERT INTO USER VALUES ('S10001','" + path + "','" + name + "','" + ic + "','" + gen + "','" + dob + "','" + nat + "','"
                    + race + "','" + reli + "','" + hp + "','" + home + "','" + email + "','" + fadd + "','" + fpost + "','" + fcity + "','" + fsta + "','NULL','0','NULL','NULL')";
        } else {
            sql = "INSERT INTO USER VALUES ('S10001','" + path + "','" + name + "','" + ic + "','" + gen + "','" + dob + "','" + nat + "','"
                    + race + "','" + reli + "','" + hp + "','" + home + "','" + email + "','" + fadd + "','" + fpost + "','" + fcity + "','"
                    + fsta + "','" + sadd + "','" + spost + "','" + scity + "','" + ssta + "')";
        }
        stm.execute(sql);
        sql = "INSERT INTO USER_ACC VALUES ('S10001','" + username + "','" + password + "','Y','" + ans + "')";
        stm.execute(sql);
        stm.close();
        c.close();
    }

    public void forgetPass(String name, int newpass) throws SQLException {
        Statement stm = c.createStatement();
        String sql = "UPDATE USER_ACC SET PASSWORD = '" + newpass + "' WHERE UID = (SELECT UID from USER WHERE U_NAME = '" + name + "')";
        stm.execute(sql);
        stm.close();
        c.close();
    }

    public void inAtt(String name, String date, String in) throws SQLException {
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = "SELECT UID from USER WHERE U_NAME = '" + name + "'";
        ResultSet rs = stm.executeQuery(sql);
        rs.next();
        String id = rs.getString(1);
        sql = "INSERT INTO ATTENDANCE (`UID`, `WORK_DATE`, `TIME_IN`) VALUES ('" + id + "','" + date + "','" + in + "')";
        stm.execute(sql);
        stm.close();
        c1.close();
    }

    public void upAtt(String name, String out) throws SQLException {
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = "SELECT UID from USER WHERE U_NAME = '" + name + "'";
        ResultSet rs = stm.executeQuery(sql);
        rs.next();
        String id = rs.getString(1);
        LocalDate current = LocalDate.now();
        sql = "UPDATE ATTENDANCE SET TIME_OUT ='" + out + "' WHERE UID ='" + id + "' AND WORK_DATE = '" + current.toString() + "'";
        stm.execute(sql);
        stm.close();
        c1.close();
    }

    public void upPass(String password) throws SQLException {
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = "UPDATE USER_ACC SET PASSWORD ='" + password + "' WHERE UID ='" + UID + "'";
        stm.execute(sql);
        stm.close();
        c1.close();
    }

    public void insertLeave(String name, String date, String type, String desc) throws SQLException {
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = "SELECT UID from USER WHERE U_NAME = '" + name + "'";
        ResultSet rs = stm.executeQuery(sql);
        rs.next();
        String aUID = rs.getString(1);
        sql = "INSERT INTO APPLYLEAVE VALUES ('" + date + "','" + type + "','" + desc + "','" + aUID + "')";
        stm.execute(sql);
        stm.close();
        c1.close();
    }

    public void upAdmin(boolean check, String name) throws SQLException {
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = "SELECT UID FROM USER WHERE U_NAME = '" + name + "'";
        ResultSet rs = stm.executeQuery(sql);
        rs.next();
        String aUID = rs.getString(1);
        if (check == true) {
            sql = "UPDATE USER_ACC SET VER_ADMIN = 'Y' WHERE UID ='" + aUID + "'";
        } else {
            sql = "UPDATE USER_ACC SET VER_ADMIN = 'N' WHERE UID ='" + aUID + "'";
        }
        stm.execute(sql);
        stm.close();
        c1.close();
    }

    public List<User> monReport(String date) throws SQLException, FileNotFoundException {
        file = new File(System.getProperty("user.home") + "/db/system.txt");
        input = new Scanner(file);
        start = input.nextInt();
        end = input.nextInt();
        List<String> id = new ArrayList<String>();
        List<User> finaldata = FXCollections.observableArrayList();
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = "SELECT UID FROM USER ORDER BY UID ASC";
        ResultSet rs = stm.executeQuery(sql);
        while (rs.next()) {
            id.add(rs.getString(1));
        }
        for (int x = 0; x < id.size(); x++) {
            List<User> data = FXCollections.observableArrayList();
            sql = "SELECT TIME_IN, TIME_OUT FROM ATTENDANCE WHERE UID = '" + id.get(x) + "' AND WORK_DATE LIKE '" + date + "%'";
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                String out = rs.getString(2);
                String yn = "YES";
                boolean check = Integer.parseInt(rs.getString(1).substring(0, 2)) > start - 1;
                if (out != null) {
                    if (check) {
                        if (Integer.parseInt(rs.getString(2).substring(0, 2)) > end) {
                            data.add(new User("", rs.getString(1), rs.getString(2), String.valueOf(Integer.parseInt(rs.getString(2).substring(0, 2)) - end), yn));
                        } else {
                            data.add(new User("", rs.getString(1), rs.getString(2), String.valueOf(0), yn));
                        }
                    } else {
                        if (Integer.parseInt(rs.getString(2).substring(0, 2)) > end) {
                            data.add(new User("", rs.getString(1), rs.getString(2), String.valueOf(Integer.parseInt(rs.getString(2).substring(0, 2)) - end), ""));
                        } else {
                            data.add(new User("", rs.getString(1), rs.getString(2), String.valueOf(0), ""));
                        }
                    }
                } else {
                    if (check) {
                        data.add(new User("", rs.getString(1), rs.getString(2), String.valueOf(0), yn));
                    } else {
                        data.add(new User("", rs.getString(1), rs.getString(2), String.valueOf(0), ""));
                    }
                }
            }
            int total = 0;
            int late = 0;
            for (int y = 0; y < data.size(); y++) {
                total += Integer.parseInt(data.get(y).getOT());
                if (data.get(y).getLate().equals("YES")) {
                    late++;
                }
            }
            try {
                sql = "SELECT u.U_NAME, COUNT(a.L_DATE) FROM APPLYLEAVE a NATURAL JOIN USER u WHERE a.UID = '" + id.get(x) + "'";
                rs = stm.executeQuery(sql);
                rs.next();
                finaldata.add(new User(rs.getString(1), data.size(), late, total, rs.getInt(2)));
            } catch (Exception e) {
                sql = "SELECT U_NAME FROM USER WHERE UID = '" + id.get(x) + "'";
                rs = stm.executeQuery(sql);
                rs.next();
                finaldata.add(new User(rs.getString(1), data.size(), late, total, 0));
            }
        }
        return finaldata;
    }

    public void upStaff(String path, String name, String ic, String gen, String dob, String nat, String race, String reli, String hp, String home,
            String email, String fadd, String fpost, String fcity, String fsta, String sadd, String spost, String scity, String ssta) throws SQLException, IOException {
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql;
        if (!path.contains("\\db\\default.png")) {
            int dot = path.lastIndexOf(".");
            Path source = Paths.get(path);
            Path destination = Paths.get(System.getProperty("user.home")+ "/db/" + UID + path.substring(dot));
            path = UID + path.substring(dot);
            Files.copy(source, destination, StandardCopyOption.REPLACE_EXISTING);
        }
        else {
            File files = new File(path);
            path = files.getName();
        }
        if (sadd == null) {
            sql = "UPDATE USER SET U_IMG = '" + path + "',U_NAME = '" + name + "',U_IC = '" + ic + "',U_GEN = '" + gen + "',U_DOB = '" + dob + "',U_NATION = '" + nat + "',U_RACE = '"
                    + race + "',U_RELI = '" + reli + "',U_HP = '" + hp + "',U_HOME = '" + home + "',U_EMAIL = '" + email + "',U_FADD = '" + fadd + "',U_FPOST = '" + fpost + "',U_FCITY = '" + fcity + "',U_FSTA = '" + fsta + "' WHERE UID = '"
                    + UID + "'";
        } else {
            sql = "UPDATE USER SET U_IMG = '" + path + "',U_NAME = '" + name + "',U_IC = '" + ic + "',U_GEN = '" + gen + "',U_DOB = '" + dob + "',U_NATION = '" + nat + "',U_RACE = '"
                    + race + "',U_RELI = '" + reli + "',U_HP = '" + hp + "',U_HOME = '" + home + "',U_EMAIL = '" + email + "',U_FADD = '" + fadd + "',U_FPOST = '" + fpost + "',U_FCITY = '" + fcity + "',U_FSTA = '" + fsta
                    + "',U_SADD = '" + sadd + "',U_SPOST = '" + spost + "',U_SCITY = '" + scity + "',U_SSTA = '" + ssta + "' WHERE UID = '" + UID + "'";
        }
        stm.execute(sql);
        stm.close();
        c1.close();
    }

    public void delStaff(String name) throws SQLException {
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = "SELECT UID from USER WHERE U_NAME = '" + name + "'";
        ResultSet rs = stm.executeQuery(sql);
        rs.next();
        String aUID = rs.getString(1);
        sql = "DELETE from APPLYLEAVE WHERE UID = '" + aUID + "'";
        stm.execute(sql);
        sql = "DELETE from ATTENDANCE WHERE UID = '" + aUID + "'";
        stm.execute(sql);
        sql = "DELETE from USER_ACC WHERE UID = '" + aUID + "'";
        stm.execute(sql);
        sql = "DELETE from USER WHERE UID = '" + aUID + "'";
        stm.execute(sql);
        stm.close();
        c1.close();
    }

    public PieDataset leavepie(String year) throws SQLException {
        DefaultPieDataset dataset = new DefaultPieDataset();
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = "SELECT COUNT(L_TYPE) FROM APPLYLEAVE WHERE L_DATE LIKE '" + year + "%' AND L_TYPE IN ('Emergency Leave','Holiday','Maternity Leave','Medical Leave','Meeting With Client','Training')";
        ResultSet rs = stm.executeQuery(sql);
        rs.next();
        int total = rs.getInt(1);
        sql = "SELECT L_TYPE, COUNT(L_TYPE) FROM APPLYLEAVE WHERE L_DATE LIKE '" + year + "%' AND L_TYPE IN ('Emergency Leave','Holiday','Maternity Leave','Medical Leave','Meeting With Client','Training') GROUP BY L_TYPE;";
        rs = stm.executeQuery(sql);
        while (rs.next()) {
            double per = (rs.getDouble(2) / total) * 100;
            dataset.setValue(rs.getString(1).concat("\n" + String.format("%.2f", per) + "%"), per);
        }
        stm.close();
        c1.close();
        return dataset;
    }

    public List<List<String>> tbl(String asql, String bsql) throws SQLException, FileNotFoundException {
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        List<String> name = new ArrayList<String>();
        List<List<String>> full = new ArrayList<List<String>>();
        String sql = "SELECT COUNT(UID) FROM USER";
        ResultSet rs = stm.executeQuery(sql);
        rs.next();
        int size = rs.getInt(1);
        sql = "SELECT U_NAME FROM USER ORDER BY UID ASC";
        rs = stm.executeQuery(sql);
        while (rs.next()) {
            name.add(rs.getString(1));
        }
        for (int x = 0; x < size; x++) {
            List<String> nametime = new ArrayList<String>();
            nametime.add(name.get(x));
            for (int y = 1; y <= 12; y++) {
                if (y < 10) {
                    sql = asql + "0" + y + "%' AND u.U_NAME = '" + name.get(x) + "'";
                } else {
                    sql = asql + y + "%' AND u.U_NAME = '" + name.get(x) + "'";
                }
                rs = stm.executeQuery(sql);
                if (bsql != "ot") {
                    rs.next();
                    if (rs.getString(1) != null) {
                        nametime.add(rs.getString(1));
                    } else {
                        nametime.add(String.valueOf("0"));
                    }
                } else {
                    file = new File(System.getProperty("user.home") + "/db/system.txt");
                    input = new Scanner(file);
                    input.nextLine();
                    int time = input.nextInt();
                    int total = 0;
                    while (rs.next()) {
                        if (rs.getString(1) != null) {
                            total = Integer.parseInt(rs.getString(1).substring(0, 2)) - time + total;
                        }
                    }
                    nametime.add(String.valueOf(total));
                }
            }
            int total = 0;
            for (int z = 1; z <= 12; z++) {
                total += Integer.parseInt(nametime.get(z));
            }
            nametime.add(String.valueOf(total));
            full.add(nametime);
        }
        return full;
    }

    public List<List<String>> leavetbl(String year) throws SQLException, FileNotFoundException {
        String asql = "SELECT COUNT(u.U_NAME) FROM APPLYLEAVE a NATURAL JOIN USER u WHERE a.L_DATE LIKE '" + year + "-";
        String bsql = "leave";
        return tbl(asql, bsql);
    }

    public XYSeriesCollection xyline(List<List<String>> data) {
        XYSeriesCollection my_data_series = new XYSeriesCollection();
        for (int y = 0; y < data.size(); y++) {
            XYSeries xydata = new XYSeries(data.get(y).get(0));
            for (int x = 1; x <= 12; x++) {
                try {
                    xydata.add(x, Integer.parseInt(data.get(y).get(x)));
                } catch (Exception e) {
                    xydata.add(x, Double.parseDouble(data.get(y).get(x)));
                }
            }
            my_data_series.addSeries(xydata);
        }
        return my_data_series;
    }

    public CategoryDataset bar(String asql, String bsql, String type) throws SQLException, FileNotFoundException {
        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
        Connection c1 = DriverManager.getConnection(url, user, pass);
        Statement stm = c1.createStatement();
        String sql = null;
        ResultSet rs = null;
        if (type != "OT") {
            for (int x = 1; x <= 12; x++) {
                if (x < 10) {
                    sql = asql + x + "%'";
                } else {
                    sql = bsql + x + "%'";
                }
                rs = stm.executeQuery(sql);
                rs.next();
                dataSet.addValue(rs.getInt(1), type, String.valueOf(x));
            }
        } else {
            file = new File(System.getProperty("user.home") + "/db/system.txt");
            input = new Scanner(file);
            input.nextLine();
            int time = input.nextInt();
            for (int x = 1; x <= 12; x++) {
                if (x < 10) {
                    sql = asql + x + "%'";
                } else {
                    sql = bsql + x + "%'";
                }
                int total = 0;
                rs = stm.executeQuery(sql);
                while (rs.next()) {
                    total = Integer.parseInt(rs.getString(1).substring(0, 2)) - time + total;
                }
                dataSet.addValue(total, type, String.valueOf(x));
            }
        }
        return dataSet;
    }

    public CategoryDataset latebar(String year) throws FileNotFoundException, SQLException {
        file = new File(System.getProperty("user.home") + "/db/system.txt");
        input = new Scanner(file);
        String time = String.valueOf(input.nextInt() - 1);
        String asql = "SELECT COUNT(TIME_IN) FROM ATTENDANCE WHERE TIME_IN > '" + time + ":59:59' AND WORK_DATE LIKE '" + year + "-0";
        String bsql = "SELECT COUNT(TIME_IN) FROM ATTENDANCE WHERE TIME_IN > '" + time + ":59:59' AND WORK_DATE LIKE '" + year + "-";
        return bar(asql, bsql, "LATE");
    }

    public List<List<String>> latetbl(String year) throws SQLException, FileNotFoundException {
        file = new File(System.getProperty("user.home") + "/db/system.txt");
        input = new Scanner(file);
        String time = String.valueOf(input.nextInt() - 1);
        String asql = "SELECT COUNT(a.TIME_IN) FROM ATTENDANCE a NATURAL JOIN USER u WHERE a.TIME_IN > '" + time + ":59:59' AND a.WORK_DATE LIKE '" + year + "-";
        String bsql = "late";
        return tbl(asql, bsql);
    }

    public CategoryDataset otbar(String year) throws FileNotFoundException, SQLException {
        file = new File(System.getProperty("user.home") + "/db/system.txt");
        input = new Scanner(file);
        input.nextLine();
        String time = input.nextLine();
        String asql = "SELECT TIME_OUT FROM ATTENDANCE WHERE TIME_OUT > '" + time + ":00:00' AND WORK_DATE LIKE '" + year + "-0";
        String bsql = "SELECT TIME_OUT FROM ATTENDANCE WHERE TIME_OUT > '" + time + ":00:00' AND WORK_DATE LIKE '" + year + "-";
        return bar(asql, bsql, "OT");
    }

    public List<List<String>> ottbl(String year) throws FileNotFoundException, SQLException {
        file = new File(System.getProperty("user.home") + "/db/system.txt");
        input = new Scanner(file);
        input.nextLine();
        String time = input.nextLine();
        String asql = "SELECT a.TIME_OUT FROM ATTENDANCE a NATURAL JOIN USER u WHERE a.TIME_OUT > '" + time + ":00:00' AND a.WORK_DATE LIKE '" + year + "-";
        String bsql = "ot";
        return tbl(asql, bsql);
    }

    public List<List<String>> atttbl(String year) throws FileNotFoundException, SQLException {
        String asql = "SELECT COUNT(a.WORK_DATE) FROM ATTENDANCE a NATURAL JOIN USER u WHERE a.WORK_DATE LIKE '" + year + "-";
        String bsql = "att";
        return tbl(asql, bsql);
    }

    public List<List<String>> bonustbl(String year, List<List<String>> att) throws FileNotFoundException {
        LocalDate start = LocalDate.of(Integer.parseInt(year), 1, 1);
        LocalDate end = start.with(lastDayOfYear());
        end = end.plusDays(1);
        int day = 0;
        while (start.isBefore(end)) {
            if (!(start.getDayOfWeek().equals(DayOfWeek.SATURDAY) || start.getDayOfWeek().equals(DayOfWeek.SUNDAY))) {
                day++;
            }
            start = start.plusDays(1);
        }
        File file = new File(System.getProperty("user.home") + "/db/system.txt");
        Scanner input = new Scanner(file);
        String hol = null;
        boolean find = true;
        List<Integer> index = new ArrayList<Integer>();
        while (input.hasNext() && find) {
            String holtime = input.nextLine();
            if (holtime.contains(String.valueOf(year))) {
                find = false;
                for (int i = -1; (i = holtime.indexOf(" ", i + 1)) != -1;) {
                    index.add(i);
                }
                hol = holtime.substring(index.get(0) + 1, index.get(1));
            }
        }
        day = day - Integer.parseInt(hol);
        List<List<String>> alldata = new ArrayList<List<String>>();
        for (int x = 0; x < att.size(); x++) {
            List<String> data = new ArrayList<String>();
            data.add(att.get(x).get(0));
            double per = ((Double.parseDouble(att.get(x).get(13)) / day) * 100);
            data.add(String.valueOf(per) + "%");
            String bonus;
            if (per == 100) {
                bonus = "2-Months Bonus";
            } else if (per >= 75) {
                bonus = "1-Months Bonus";
            } else {
                bonus = "No Bonus";
            }
            data.add(bonus);
            alldata.add(data);
        }
        return alldata;
    }
}
