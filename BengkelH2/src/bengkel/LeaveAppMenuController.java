/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bengkel;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.util.Duration;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Windows-8.1
 */
public class LeaveAppMenuController implements Initializable {

    DBConnection db;
    GUIStyle gui;
    ObservableList<String> type = FXCollections.observableArrayList("1. Emergency Leave", "2. Holiday", "3. Maternity Leave",
            "4. Medical Leave", "5. Meeting With Client", "6. Training");

    @FXML
    private ChoiceBox<String> lamName;

    @FXML
    private DatePicker lamDate;

    @FXML
    private DatePicker lamToDate;

    @FXML
    private ChoiceBox<String> lamType;

    @FXML
    private TextArea lamDesc;

    @FXML
    private Button lamSub;

    @FXML
    private Button lamCan;

    @FXML
    private Hyperlink laBack;

    @FXML
    private Label Time;

    public LeaveAppMenuController() throws SQLException, ClassNotFoundException {
        db = new DBConnection();
        gui = new GUIStyle();
        bindToTime();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        lamType.setItems(type);
        try {
            lamName.setItems(db.name("leave"));
        } catch (SQLException ex) {
        }
    }

    public void backListener() throws Exception {
        String fxml = "MainMenu.fxml";
        gui.chgScene(fxml);
    }

    public void cancelListener() throws Exception {
        lamName.setValue(null);
        lamDate.setValue(null);
        lamToDate.setValue(null);
        lamDesc.clear();
        lamType.setValue(null);
    }

    public void submitListener() throws Exception {
        if (lamName.getValue() == null || lamDate.getValue() == null || lamType.getValue() == null) {
            JOptionPane.showMessageDialog(null, "Please fill in all", "Warning", JOptionPane.ERROR_MESSAGE);
        }
        else if (lamDate.getValue().isBefore(LocalDate.now().plusDays(4))&&lamType.getValue().equals("2. Holiday")) {
            JOptionPane.showMessageDialog(null, "Holidays Can Be Apply After 3 days", "Warning", JOptionPane.ERROR_MESSAGE);
            lamDate.setValue(null);
        }
        else if (lamToDate.getValue() != null) {
            if (lamToDate.getValue().isAfter(lamDate.getValue())) {
                int test = 0;
                LocalDate firstdate = lamDate.getValue();
                String appdate = null;
                while (!firstdate.isAfter(lamToDate.getValue()) && test != 1) {
                    test = db.checkLeave(firstdate.toString(), lamName.getValue());
                    appdate = firstdate.toString();
                    firstdate = firstdate.plusDays(1);
                }
                if (test != 1) {
                    FileWriter fwrite = new FileWriter(System.getProperty("user.home")+"/db/leave.txt", true);
                    PrintWriter out = new PrintWriter(fwrite);
                    firstdate = lamDate.getValue();
                    while (!firstdate.isAfter(lamToDate.getValue())) {
                        out.println(lamName.getValue());
                        out.println(firstdate.toString());
                        out.println(lamType.getValue().substring(3));
                        if (lamDesc.getText().isEmpty()) {
                            out.println("NULL");
                        } else {
                            out.println(lamDesc.getText());
                        }
                        firstdate = firstdate.plusDays(1);
                    }
                    out.close();
                    JOptionPane.showMessageDialog(null, "Leave Submited");
                    lamName.setValue(null);
                    lamDate.setValue(null);
                    lamToDate.setValue(null);
                    lamDesc.clear();
                    lamType.setValue(null);
                } else {
                    JOptionPane.showMessageDialog(null, "Leave Had Been Applied On " + appdate);
                    lamDate.setValue(null);
                    lamToDate.setValue(null);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Second Date Must Be Larger Than First Date", "Warning", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            if (db.checkLeave(lamDate.getValue().toString(), lamName.getValue()) != 1) {
                FileWriter fwrite = new FileWriter(System.getProperty("user.home")+"/db/leave.txt", true);
                PrintWriter out = new PrintWriter(fwrite);
                out.println(lamName.getValue());
                out.println(lamDate.getValue().toString());
                out.println(lamType.getValue().substring(3));
                if (lamDesc.getText().isEmpty()) {
                    out.println("NULL");
                } else {
                    out.println(lamDesc.getText());
                }
                out.close();
                JOptionPane.showMessageDialog(null, "Leave Submited");
                lamName.setValue(null);
                lamDate.setValue(null);
                lamDesc.clear();
                lamType.setValue(null);
            } else {
                JOptionPane.showMessageDialog(null, "Leave Had Been Applied On That Day");
                lamDate.setValue(null);
            }
        }
    }

    private void bindToTime() {
        Timeline timeline;
        timeline = new Timeline(
                new KeyFrame(Duration.seconds(0), (ActionEvent actionEvent) -> {
                    Calendar time = Calendar.getInstance();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss aa");
                    Time.setText(simpleDateFormat.format(time.getTime()));
                }),
                new KeyFrame(Duration.seconds(1))
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }
}
