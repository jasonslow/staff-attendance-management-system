/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bengkel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.util.Callback;
import javafx.util.Duration;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Windows-8.1
 */
public class SettingsController implements Initializable {

    DBConnection db;
    GUIStyle gui;
    File file;
    Scanner input;
    ObservableList<User> verify;

    @FXML
    private Label sTime;
    @FXML
    private Hyperlink sBack;
    @FXML
    private TableView<User> sTbl;
    @FXML
    private TableColumn<User, String> sName;
    @FXML
    private TableColumn<User, Boolean> sAdmin;
    @FXML
    private Button sSave;
    @FXML
    private Button sCancel;
    @FXML
    private TextField ssStart;
    @FXML
    private TextField ssEnd;
    @FXML
    private Button ssSave;
    @FXML
    private CheckBox ssCheck;
    @FXML
    private ChoiceBox<String> ssStaff;
    @FXML
    private Button ssDelete;
    @FXML
    private DatePicker pub1;
    @FXML
    private DatePicker pub2;
    @FXML
    private DatePicker pub3;
    @FXML
    private DatePicker pub4;
    @FXML
    private DatePicker pub5;
    @FXML
    private DatePicker pub6;
    @FXML
    private DatePicker pub7;
    @FXML
    private DatePicker pub8;
    @FXML
    private DatePicker pub9;
    @FXML
    private CheckBox che2;
    @FXML
    private CheckBox che1;
    @FXML
    private DatePicker pubop1;
    @FXML
    private DatePicker pubop2;
    @FXML
    private Button phSave;

    public SettingsController() throws SQLException, FileNotFoundException, ClassNotFoundException {
        db = new DBConnection();
        gui = new GUIStyle();
        verify = db.checkAdmin();
        file = new File(System.getProperty("user.home")+"/db/system.txt");
        input = new Scanner(file);
        bindToTime();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        sName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        sAdmin.setCellValueFactory(
                new Callback<CellDataFeatures<User, Boolean>, ObservableValue<Boolean>>() {
                    @Override
                    public ObservableValue<Boolean> call(CellDataFeatures<User, Boolean> param) {
                        return param.getValue().adminProperty();
                    }
                });
        sAdmin.setCellFactory(tc -> new CheckBoxTableCell<>());
        sTbl.setItems(verify);
        ssStaff.setDisable(true);
        pubop1.setDisable(true);
        pubop2.setDisable(true);
        try {
            ssStaff.setItems(db.name("settings"));
        } catch (SQLException ex) {
        }
        ssStart.setText(String.valueOf(input.nextInt()));
        ssEnd.setText(String.valueOf(input.nextInt()));
        boolean test = true;
        while (input.hasNext()&&test) {
            String output = input.nextLine();
            if (output.contains(String.valueOf(LocalDate.now().getYear()))) {
                test = false;
                List<Integer> index = new ArrayList<Integer>();
                for (int i = -1; (i = output.indexOf(" ", i + 1)) != -1; ) {
                    index.add(i);
                }
                pub1.setValue(LocalDate.parse(output.substring(index.get(1) + 1, index.get(2))));
                pub2.setValue(LocalDate.parse(output.substring(index.get(2) + 1, index.get(3))));
                pub3.setValue(LocalDate.parse(output.substring(index.get(3) + 1, index.get(4))));
                pub4.setValue(LocalDate.parse(output.substring(index.get(4) + 1, index.get(5))));
                pub5.setValue(LocalDate.parse(output.substring(index.get(5) + 1, index.get(6))));
                pub6.setValue(LocalDate.parse(output.substring(index.get(6) + 1, index.get(7))));
                pub7.setValue(LocalDate.parse(output.substring(index.get(7) + 1, index.get(8))));
                pub8.setValue(LocalDate.parse(output.substring(index.get(8) + 1, index.get(9))));
                pub9.setValue(LocalDate.parse(output.substring(index.get(9) + 1, index.get(10))));
                if (output.contains("*")) {
                    che1.setSelected(true);
                    pubop1.setDisable(false);
                    pubop1.setValue(LocalDate.parse(output.substring(index.get(10)+1, index.get(11)-1)));
                }
                else if (output.contains("&")){
                    che2.setSelected(true);
                    pubop2.setDisable(false);
                    pubop2.setValue(LocalDate.parse(output.substring(index.get(10)+1, index.get(11)-1)));
                }
        }
        }
        
    }

    public void backListener() throws Exception {
        boolean check = checkEqual();
        if (check) {
            int value = JOptionPane.showConfirmDialog(null, "There Are Changes In Your Settings.\nAre you sure want to add admin ?", "Please Confirm", JOptionPane.YES_NO_OPTION);
            if (value == 0) {
                for (int x = 0; x < verify.size(); x++) {
                    db.upAdmin(verify.get(x).getAdmin(), verify.get(x).getName());
                }
                JOptionPane.showMessageDialog(null, "Update Succesful");
                gui.chgScene("AdminMenu.fxml");
            } else {
                gui.chgScene("AdminMenu.fxml");
            }
        } else {
            gui.chgScene("AdminMenu.fxml");
        }
    }

    public void clickListener() {
        boolean check;
        int choose = sTbl.getSelectionModel().getSelectedIndex();
        if (choose != -1) {
            if (verify.get(choose).getAdmin() == false) {
                check = true;
            } else {
                check = false;
            }
            verify.set(choose, new User(verify.get(choose).getName(), check));
        } else {
            JOptionPane.showMessageDialog(null, "Please One Of The Name", "Warning", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void saveListener() throws SQLException {
        int value = JOptionPane.showConfirmDialog(null, "Are you sure want to add admin ?", "Please Confirm", JOptionPane.YES_NO_OPTION);
        if (value == JOptionPane.YES_OPTION) {
            boolean check = checkEqual();
            if (check) {
                for (int x = 0; x < verify.size(); x++) {
                    db.upAdmin(verify.get(x).getAdmin(), verify.get(x).getName());
                }
                JOptionPane.showMessageDialog(null, "Update Succesful");
            } else {
                JOptionPane.showMessageDialog(null, "No Changes");
            }
        }
    }

    public void cancelListener() throws SQLException {
        verify = db.checkAdmin();
        sTbl.setItems(verify);
    }

    public boolean checkEqual() throws SQLException {
        boolean check = false;
        int index = 0;
        int size = verify.size();
        while (index < size) {
            if (verify.get(index).getAdmin() != db.checkAdmin().get(index).getAdmin()) {
                check = true;
                index = size;
            }
            index++;
        }
        return check;
    }

    public void checkbox() {
        if (ssCheck.isSelected()) {
            int value = JOptionPane.showConfirmDialog(null, "Are You Sure You Want To Delete User.", "Warning", JOptionPane.YES_NO_OPTION);
            if (value == JOptionPane.YES_OPTION) {
                ssStaff.setDisable(false);
            } else {
                ssCheck.setSelected(false);
                ssStaff.setDisable(true);
            }
        } else {
            ssStaff.getSelectionModel().select(null);
            ssStaff.setDisable(true);
        }
    }

    public void deleteListener() throws SQLException {
        if (!ssStaff.getSelectionModel().isEmpty()) {
            int value = JOptionPane.showConfirmDialog(null, "Selected Staff Will Be Delete.\nAre You Sure?", "Warning", JOptionPane.YES_NO_OPTION);
            if (value == JOptionPane.YES_OPTION) {
                db.delStaff(ssStaff.getValue());
                JOptionPane.showMessageDialog(null, "Staff DELETED");
                ssStaff.setItems(db.name("settings"));
                verify = db.checkAdmin();
                sTbl.setItems(verify);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Select one of the name.", "Warning", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void ssSave() throws FileNotFoundException {
        if (ssStart.getText().isEmpty() || ssEnd.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Fill In The Start or End Time");
        } else if (!isValid(ssStart.getText()) && !isValid(ssEnd.getText())) {
            JOptionPane.showMessageDialog(null, "Consist of Numbers Only");
        } else if (Integer.parseInt(ssStart.getText()) >= Integer.parseInt(ssEnd.getText())) {
            JOptionPane.showMessageDialog(null, "End Time Cannot Early Than Start Time And Time Is In 24hours Format");
        } else {
            PrintWriter out = new PrintWriter(System.getProperty("user.home")+"/db/system.txt");
            out.println(ssStart.getText());
            out.println(ssEnd.getText());
            out.close();
            JOptionPane.showMessageDialog(null, "Save Successfully");
        }
    }

    public void checkpub1() {
        if (che1.isSelected()) {
            int value = JOptionPane.showConfirmDialog(null, "Do you confirm that 2 same holidays in one year ?", "Warning", JOptionPane.YES_NO_OPTION);
            if (value == JOptionPane.YES_OPTION) {
                pubop1.setDisable(false);
                che2.setSelected(false);
                pubop2.setDisable(true);
                pubop2.setValue(null);
            }
            else {
                che1.setSelected(false);
                pubop1.setDisable(true);
                pubop1.setValue(null);
            }
        } else {
                pubop1.setDisable(true);
                pubop1.setValue(null);
        }
    }

    public void checkpub2() {
        if (che2.isSelected()) {
            int value = JOptionPane.showConfirmDialog(null, "Do you confirm that 2 same holidays in one year ?", "Warning", JOptionPane.YES_NO_OPTION);
            if (value == JOptionPane.YES_OPTION) {
                pubop2.setDisable(false);
                che1.setSelected(false);
                pubop1.setDisable(true);
                pubop1.setValue(null);
            }
            else {
                che2.setSelected(false);
                pubop2.setDisable(true);
                pubop2.setValue(null);
            }
        } else {
                pubop2.setDisable(true);
                pubop2.setValue(null);
        }
    }
    
    public void phSave() throws FileNotFoundException {
        LocalDate date = LocalDate.now();
        int year = date.getYear();
        LocalDate first = date.with(firstDayOfYear());
        LocalDate last = date.with(lastDayOfYear());
        if (pub1.getValue() == null || pub2.getValue() == null || pub3.getValue() == null || pub4.getValue() == null || pub5.getValue() == null
                || pub6.getValue() == null || pub7.getValue() == null || pub8.getValue() == null || pub9.getValue() == null) {
            JOptionPane.showMessageDialog(null, "Fill In The Blank");
        } else if (pub1.getValue().isAfter(last) || pub2.getValue().isAfter(last) || pub3.getValue().isAfter(last) || pub4.getValue().isAfter(last)
                || pub5.getValue().isAfter(last) || pub6.getValue().isAfter(last) || pub7.getValue().isAfter(last) || pub8.getValue().isAfter(last) || pub9.getValue().isAfter(last)
                || pub1.getValue().isBefore(first) || pub2.getValue().isBefore(first) || pub3.getValue().isBefore(first) || pub4.getValue().isBefore(first)
                || pub5.getValue().isBefore(first) || pub6.getValue().isBefore(first) || pub7.getValue().isBefore(first) || pub8.getValue().isBefore(first) || pub9.getValue().isBefore(first)) {
            JOptionPane.showMessageDialog(null, "For " + date.getYear() + " year public holidays only");
        } else if (che1.isSelected() || che2.isSelected()) {
            if (che1.isSelected()) {
            if (pubop1.getValue() == null && che1.isSelected()) {
                JOptionPane.showMessageDialog(null, "Fill In The Blank");
            }
            else if (pubop1.getValue().isAfter(last) || pubop1.getValue().isBefore(first)) {
                JOptionPane.showMessageDialog(null, "For " + date.getYear() + " year public holidays only");
            } 
            else {
                LocalDate pubhol[] = {pub1.getValue(), pub2.getValue(), pub3.getValue(), pub4.getValue(), pub5.getValue(),
                    pub6.getValue(), pub7.getValue(), pub8.getValue(), pub9.getValue(), pubop1.getValue(),pub2.getValue().plusDays(1),pub5.getValue().plusDays(1), LocalDate.of(year, 1, 1), 
                    LocalDate.of(year, 4, 15), LocalDate.of(year, 5, 1), LocalDate.of(year, 8, 31), LocalDate.of(year, 9, 16), LocalDate.of(year, 12, 25)};
                int total = 0;
                for (int x = 0; x < pubhol.length; x++) {
                    if (!pubhol[x].getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
                        total += 1;
                    }
                }
                List<String> data = new ArrayList<String>();
                File file = new File(System.getProperty("user.home")+"/db/system.txt");
                Scanner input = new Scanner(file);
                while (input.hasNext()) {
                    String test = input.nextLine();
                    if (!test.contains(String.valueOf(year))) {
                        data.add(test);
                    }
                }
                String line = year + " " + total + " " + pubhol[0].toString() + " " + pubhol[1].toString() + " " + pubhol[2].toString() + " " + pubhol[3].toString() + " " + pubhol[4].toString() + " " + pubhol[5].toString() + " "
                        + pubhol[6].toString() + " " + pubhol[7].toString() + " " + pubhol[8].toString() + " " + pubhol[9].toString().concat("*") + " " + pubhol[10].toString() + " "
                        + pubhol[11].toString() + " " + pubhol[12].toString() + " " + pubhol[13].toString() + " " + pubhol[14].toString() + " " + pubhol[15].toString() + " "
                        + pubhol[16].toString() + " " + pubhol[17].toString();
                PrintWriter out = new PrintWriter(System.getProperty("user.home")+"/db/system.txt");
                for (int y = 0; y < data.size(); y++) {
                    out.println(data.get(y));
                }
                out.println(line);
                out.close();
                JOptionPane.showMessageDialog(null, "Save Successfully");
            }
            }
            else {
            if (pubop2.getValue() == null && che2.isSelected())
                JOptionPane.showMessageDialog(null, "Fill In The Blank");
            
            else if (pubop2.getValue().isAfter(last) || pubop2.getValue().isBefore(first)) {
                JOptionPane.showMessageDialog(null, "For " + date.getYear() + " year public holidays only");
            }
            else {
                LocalDate pubhol[] = {pub1.getValue(), pub2.getValue(), pub3.getValue(), pub4.getValue(), pub5.getValue(),
                    pub6.getValue(), pub7.getValue(), pub8.getValue(), pub9.getValue(), pubop2.getValue(), pub2.getValue().plusDays(1),pub5.getValue().plusDays(1),LocalDate.of(year, 1, 1), 
                    LocalDate.of(year, 4, 15), LocalDate.of(year, 5, 1), LocalDate.of(year, 8, 31), LocalDate.of(year, 9, 16), LocalDate.of(year, 12, 25)};
                int total = 0;
                for (int x = 0; x < pubhol.length; x++) {
                    if (!pubhol[x].getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
                        total += 1;
                    }
                }
                List<String> data = new ArrayList<String>();
                File file = new File(System.getProperty("user.home")+"/db/system.txt");
                Scanner input = new Scanner(file);
                while (input.hasNext()) {
                    String test = input.nextLine();
                    if (!test.contains(String.valueOf(year))) {
                        data.add(test);
                    }
                }
                String line = year + " " + total + " " + pubhol[0].toString() + " " + pubhol[1].toString() + " " + pubhol[2].toString() + " " + pubhol[3].toString() + " " + pubhol[4].toString() + " " + pubhol[5].toString() + " "
                        + pubhol[6].toString() + " " + pubhol[7].toString() + " " + pubhol[8].toString() + " " + pubhol[9].toString().concat("&") + " " + pubhol[10].toString() + " "
                        + pubhol[11].toString() + " " + pubhol[12].toString() + " " + pubhol[13].toString() + " " + pubhol[14].toString() + " " + pubhol[15].toString() + " "
                        + pubhol[16].toString() + " " + pubhol[17].toString();
                PrintWriter out = new PrintWriter(System.getProperty("user.home")+"/db/system.txt");
                for (int y = 0; y < data.size(); y++) {
                    out.println(data.get(y));
                }
                out.println(line);
                out.close();
                JOptionPane.showMessageDialog(null, "Save Successfully");
            }
            }
            
        } else {
            LocalDate pubhol[] = {pub1.getValue(), pub2.getValue(),pub3.getValue(), pub4.getValue(), pub5.getValue(),
                pub6.getValue(), pub7.getValue(), pub8.getValue(), pub9.getValue(),pub2.getValue().plusDays(1),pub5.getValue().plusDays(1), LocalDate.of(year, 1, 1),
                LocalDate.of(year, 4, 15), LocalDate.of(year, 5, 1), LocalDate.of(year, 8, 31), LocalDate.of(year, 9, 16), LocalDate.of(year, 12, 25)};
            int total = 0;
            for (int x = 0; x < pubhol.length; x++) {
                if (!pubhol[x].getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
                    total += 1;
                }
            }
            List<String> data = new ArrayList<String>();
            File file = new File(System.getProperty("user.home")+"/db/system.txt");
            Scanner input = new Scanner(file);
            while (input.hasNext()) {
                String test = input.nextLine();
                if (!test.contains(String.valueOf(year))) {
                    data.add(test);
                }
            }
            String line = year + " " + total + " " + pubhol[0].toString() + " " + pubhol[1].toString() + " " + pubhol[2].toString() + " " + pubhol[3].toString() + " " + pubhol[4].toString() + " " + pubhol[5].toString() + " "
                    + pubhol[6].toString() + " " + pubhol[7].toString() + " " + pubhol[8].toString() + " " + pubhol[9].toString() + " " + pubhol[10].toString() + " "
                    + pubhol[11].toString() + " " + pubhol[12].toString() + " " + pubhol[13].toString() + " " + pubhol[14].toString() + " " + pubhol[15].toString() + " " + pubhol[16].toString();
            PrintWriter out = new PrintWriter(System.getProperty("user.home")+"/db/system.txt");
            for (int y = 0; y < data.size(); y++) {
                out.println(data.get(y));
            }
            out.println(line);
            out.close();
            JOptionPane.showMessageDialog(null, "Save Successfully");
        }

    }

    public boolean isValid(String num) {
        boolean valid = true;
        int size = num.length();
        int index = 0;
        while (valid && index < size) {
            if (!Character.isDigit(num.charAt(index))) {
                valid = false;
            }
            index++;
        }
        return valid;
    }

    private void bindToTime() {
        Timeline timeline;
        timeline = new Timeline(
                new KeyFrame(Duration.seconds(0), (ActionEvent actionEvent) -> {
                    Calendar time = Calendar.getInstance();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss aa");
                    sTime.setText(simpleDateFormat.format(time.getTime()));
                }),
                new KeyFrame(Duration.seconds(1))
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }
}
