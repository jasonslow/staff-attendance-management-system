/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bengkel;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import javafx.util.Duration;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Windows-8.1
 */
public class AttendanceListController implements Initializable {

    DBConnection db;
    GUIStyle gui;
    File file;
    Scanner input;
    ObservableList<User> data;

    @FXML
    private Hyperlink alBack;

    @FXML
    private TableView<User> alTbl;

    @FXML
    private TableColumn<User, String> alName;

    @FXML
    private TableColumn<User, String> alIn;

    @FXML
    private TableColumn<User, String> alOut;

    @FXML
    private Label Time;

    public AttendanceListController() throws SQLException, FileNotFoundException, ClassNotFoundException {
        db = new DBConnection();
        gui = new GUIStyle();
        data = db.attendanceName();
        file = new File(System.getProperty("user.home")+"/db/system.txt");
        input = new Scanner(file);
        bindToTime();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        int sTime = input.nextInt();
        alName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        alIn.setCellValueFactory(cellData -> cellData.getValue().timeInProperty());
        alIn.setCellFactory(new Callback<TableColumn<User, String>, TableCell<User, String>>() {
            @Override
            public TableCell<User, String> call(TableColumn<User, String> param) {
                return new TableCell<User, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                            if (!item.equals("   -   ")) {
                                if (Integer.parseInt(item.substring(0, 2)) > sTime - 1) {
                                    this.setTextFill(Color.RED);
                                }
                            }
                        }
                        setText(item);
                    }
                };
            }
        });
        alOut.setCellValueFactory(cellData -> cellData.getValue().timeOutProperty());
        alTbl.setItems(data);

    }

    public void backListener() throws Exception {
        gui.chgScene("MainMenu.fxml");
    }

    public void clickListner(MouseEvent e) throws Exception {
        if (e.getClickCount() == 2 && alTbl.getSelectionModel().getSelectedIndex() != -1) {
            String pass = JOptionPane.showInputDialog(null, "Please Enter Your Password: ");
            if (db.checkAtt(data.get(alTbl.getSelectionModel().getSelectedIndex()).getName(), pass)) {
                Calendar time = Calendar.getInstance();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
                String str = simpleDateFormat.format(time.getTime());
                if ("   -   ".equals(data.get(alTbl.getSelectionModel().getSelectedIndex()).getTimeIn())) {
                    int num = alTbl.getSelectionModel().getSelectedIndex();
                    data.set(alTbl.getSelectionModel().getSelectedIndex(),
                            new User(data.get(alTbl.getSelectionModel().getSelectedIndex()).getName(), str, "   -   "));
                    if (Integer.parseInt(str.substring(0, 1)) > 8) {
                        data.get(alTbl.getSelectionModel().getSelectedIndex()).getName();
                    }
                    db.setAttend(data);
                    Date date = new Date();
                    SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
                    db.inAtt(data.get(num).getName(), ft.format(date), data.get(num).getTimeIn());
                    JOptionPane.showMessageDialog(null, "Check In " + str);
                } else if ("   -   ".equals(data.get(alTbl.getSelectionModel().getSelectedIndex()).getTimeOut())
                        || data.get(alTbl.getSelectionModel().getSelectedIndex()).getTimeOut() == null) {
                    int num = alTbl.getSelectionModel().getSelectedIndex();
                    data.set(alTbl.getSelectionModel().getSelectedIndex(),
                            new User(data.get(alTbl.getSelectionModel().getSelectedIndex()).getName(),
                                    data.get(alTbl.getSelectionModel().getSelectedIndex()).getTimeIn(), str));
                    db.setAttend(data);
                    db.upAtt(data.get(num).getName(), data.get(num).getTimeOut());
                    JOptionPane.showMessageDialog(null, "Check Out " + str);
                } else {
                    JOptionPane.showMessageDialog(null, "U have Check Out !!!");
                }
            } else if (pass == null) {
            } else {
                JOptionPane.showMessageDialog(null, "Wrong Password", "Warning", JOptionPane.ERROR_MESSAGE);
            }
        } else if (e.getClickCount() == 2 && alTbl.getSelectionModel().getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(null, "Please One Of The Name", "Warning", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void bindToTime() {
        Timeline timeline;
        timeline = new Timeline(
                new KeyFrame(Duration.seconds(0), (ActionEvent actionEvent) -> {
                    Calendar time = Calendar.getInstance();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss aa");
                    Time.setText(simpleDateFormat.format(time.getTime()));
                }),
                new KeyFrame(Duration.seconds(1))
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }
}
