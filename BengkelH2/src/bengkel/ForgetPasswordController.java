/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bengkel;

import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Windows-8.1
 */
public class ForgetPasswordController implements Initializable {

    DBConnection db;
    GUIStyle gui;
    
    @FXML
    private Label Time;
    @FXML
    private TabPane tab;
    @FXML
    private Tab tab1;
    @FXML
    private Tab tab2;
    @FXML
    private Tab tab3;
    @FXML
    private TextField fpName;
    @FXML
    private TextField fpIC;
    @FXML
    private Button fpCan1;
    @FXML
    private Button fpNext1;
    @FXML
    private TextField fpAns;
    @FXML
    private Button fpCan2;
    @FXML
    private Button fpNext2;
    @FXML
    private Button fpBack;
    @FXML
    private Label Done;
    @FXML
    private Label label1;
    @FXML
    private Label label2;
    @FXML
    private Button fpFin;

    public ForgetPasswordController() throws SQLException, ClassNotFoundException {
        db = new DBConnection();
        gui = new GUIStyle();
    }
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        bindToTime();
        tab2.setDisable(true);
        tab3.setDisable(true);
    }    
    
    public void finListener() {
        gui.clearStage();
    }
    
    public void cancelListener() {
        Done.setText("");
        label1.setText("Failed");
        label2.setText("");
        tab3.setDisable(false);
        tab1.setDisable(true);
        tab2.setDisable(true);
        tab.getSelectionModel().selectLast();
    }
    
    public void backListener() {
        if (tab.getSelectionModel().getSelectedIndex() == 1) { 
            tab1.setDisable(false);
            tab.getSelectionModel().selectPrevious();
            tab2.setDisable(true);
        }
    }
    
    public void nextListener() throws SQLException {
        int selected = tab.getSelectionModel().getSelectedIndex();
        if (selected == 0) {
            checkNameIc();
        }
        else
            checkAns();
    }
    
    public void checkNameIc() throws SQLException {
        if (fpName.getText().isEmpty() || fpIC.getText().isEmpty())
            JOptionPane.showMessageDialog(null, "Please Fill In All The Blank");
        else if (db.CheckNameIC(fpName.getText(), fpIC.getText())) {
            JOptionPane.showMessageDialog(null, "Wrong Name or IC / Passport");
            fpName.clear();
            fpIC.clear();
        }
        else {
            tab2.setDisable(false);
            tab.getSelectionModel().selectNext();
            tab1.setDisable(true);
        }
    }
    
    public void checkAns() throws SQLException {
        if (fpAns.getText().isEmpty())
            JOptionPane.showMessageDialog(null, "Please Fill In The Blank");
        else if (db.CheckAns(fpAns.getText())) {
            JOptionPane.showMessageDialog(null, "Wrong Answer");
            fpAns.clear();
        }
        else {
            Random random = new Random();
            int number = random.nextInt(100000) + 10000;
            String str = String.valueOf(number);
            label2.setText(str);
            tab3.setDisable(false);
            tab.getSelectionModel().selectNext();
            tab2.setDisable(true);
            db.forgetPass(fpName.getText(), number);
        }
    }
    
    private void bindToTime() {
        Timeline timeline;
        timeline = new Timeline(
                new KeyFrame(Duration.seconds(0), (ActionEvent actionEvent) -> {
                    Calendar time = Calendar.getInstance();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss aa");
                    Time.setText(simpleDateFormat.format(time.getTime()));
                }),
                new KeyFrame(Duration.seconds(1))
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }
}
