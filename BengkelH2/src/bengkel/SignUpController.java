/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bengkel;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Windows-8.1
 */
public class SignUpController implements Initializable {

    DBConnection db;
    GUIStyle gui;
    Image image;
    File file;
    ObservableList<String> gender = FXCollections.observableArrayList("M", "F");
    ObservableList<String> nation = FXCollections.observableArrayList("Malaysian", "Non-Malaysian");
    ObservableList<String> state = FXCollections.observableArrayList("WPKL", "Selangor", "Melaka", "Perak", "Negeri Sembilan",
            "Pahang", "Johor", "Kedah", "Terengganu", "Pulau Pinang",
            "Perlis", "Kelantan", "Sabah", "Sarawak", "Labuan", "Putrajaya");

    @FXML
    private Label Time;
    @FXML
    private TabPane tab;
    @FXML
    private Tab tab1;
    @FXML
    private Tab tab2;
    @FXML
    private Tab tab3;
    @FXML
    private Tab tab4;
    @FXML
    private Tab tab5;
    @FXML
    private Tab tab6;
    @FXML
    private TextField suUser;
    @FXML
    private PasswordField suPass;
    @FXML
    private PasswordField suCPass;
    @FXML
    private TextField suAns;
    @FXML
    private Button suCan1;
    @FXML
    private Button suNext1;
    @FXML
    private ImageView suPic;
    @FXML
    private Hyperlink suUp;
    @FXML
    private Button suCan2;
    @FXML
    private Button suNext2;
    @FXML
    private Button suBack1;
    @FXML
    private TextField suName;
    @FXML
    private TextField suIC;
    @FXML
    private DatePicker suDOB;
    @FXML
    private ChoiceBox<String> suGen;
    @FXML
    private ChoiceBox<String> suNat;
    @FXML
    private Button suCan3;
    @FXML
    private Button suNext3;
    @FXML
    private Button suBack2;
    @FXML
    private TextField suRace;
    @FXML
    private TextField suReli;
    @FXML
    private TextField suHP;
    @FXML
    private TextField suHome;
    @FXML
    private TextField suEmail;
    @FXML
    private Button suCan4;
    @FXML
    private Button suNext4;
    @FXML
    private Button suBack3;
    @FXML
    private TextArea suPAdd;
    @FXML
    private TextField suPPost;
    @FXML
    private TextField suPCity;
    @FXML
    private ChoiceBox<String> suPSta;
    @FXML
    private TextArea suSAdd;
    @FXML
    private TextField suSPost;
    @FXML
    private TextField suSCity;
    @FXML
    private ChoiceBox<String> suSSta;
    @FXML
    private Button suCan5;
    @FXML
    private Button suNext5;
    @FXML
    private Button suBack4;
    @FXML
    private Label suSuccess;
    @FXML
    private Button suFinish;

    public SignUpController() throws SQLException, ClassNotFoundException {
        db = new DBConnection();
        gui = new GUIStyle();
        file = new File(System.getProperty("user.home") + "/db/default.png");
        image = new Image(file.toURI().toString());
        bindToTime();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        suPic.setImage(image);
        suGen.setItems(gender);
        suNat.setItems(nation);
        suPSta.setItems(state);
        suSSta.setItems(state);
        tab2.setDisable(true);
        tab3.setDisable(true);
        tab4.setDisable(true);
        tab5.setDisable(true);
        tab6.setDisable(true);
    }

    public void finListener() {
        gui.clearStage();
    }

    public void cancelListener() {
        tab6.setDisable(false);
        suSuccess.setText("Failed");
        tab1.setDisable(true);
        tab2.setDisable(true);
        tab3.setDisable(true);
        tab4.setDisable(true);
        tab5.setDisable(true);
        tab.getSelectionModel().selectLast();
    }

    public void backListener() {
        int selected = tab.getSelectionModel().getSelectedIndex();
        if (selected == 1) {
            tab1.setDisable(false);
            tab.getSelectionModel().selectPrevious();
            tab2.setDisable(true);
        } else if (selected == 2) {
            tab2.setDisable(false);
            tab.getSelectionModel().selectPrevious();
            tab3.setDisable(true);
        } else if (selected == 3) {
            tab3.setDisable(false);
            tab.getSelectionModel().selectPrevious();
            tab4.setDisable(true);
        } else if (selected == 4) {
            tab4.setDisable(false);
            tab.getSelectionModel().selectPrevious();
            tab5.setDisable(true);
        }
    }

    public void nextListener() throws SQLException, IOException {
        int selected = tab.getSelectionModel().getSelectedIndex();
        if (selected == 0) {
            checkAcc();
        } else if (selected == 1) {
            if (image.isError()) {
                JOptionPane.showMessageDialog(null, "Please Choose A Suitable File Type Photo");
            } else {
                tab3.setDisable(false);
                tab.getSelectionModel().selectNext();
                tab2.setDisable(true);
            }
        } else if (selected == 2) {
            if (suName.getText().isEmpty() || suIC.getText().isEmpty() || suDOB.getValue() == null || suGen.getSelectionModel().isEmpty() || suNat.getSelectionModel().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Please Fill In All The Blank");
            } else if (suName.getText().length() > 99 || suIC.getText().length() > 99) {
                JOptionPane.showMessageDialog(null, "Limit To 99 Characters Only");
                if (suName.getText().length() > 99) {
                    suName.setText(null);
                } else {
                    suIC.setText(null);
                }
            } 
            else if (!db.CheckNameIC(null, suIC.getText())) {
                JOptionPane.showMessageDialog(null, "Same IC or Passport Number Found");
                suIC.setText(null);
            }
            else {
                tab4.setDisable(false);
                tab.getSelectionModel().selectNext();
                tab3.setDisable(true);
            }
        } else if (selected == 3) {
            checkBio();
        } else {
            checkAdd();
        }
    }

    public void uploadPic() {
        FileChooser chooser = new FileChooser();
        try {
            file = new File(chooser.showOpenDialog(null).getPath());
            image = new Image(file.toURI().toString());
            suPic.setImage(image);
        } catch (NullPointerException e) {
        }
    }

    public void checkAcc() throws SQLException {
        if (suUser.getText().isEmpty() || suPass.getText().isEmpty() || suCPass.getText().isEmpty() || suAns.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please Fill In All The Blank");
        } else if (suUser.getText().length() > 99) {
            JOptionPane.showMessageDialog(null, "Username Limit 99 Characters Only");
            suUser.clear();
        } else if (db.checkUsername(suUser.getText()) == false) {
            JOptionPane.showMessageDialog(null, "Username Had Been Used");
            suUser.clear();
        } else if (suPass.getText().length() < 5) {
            JOptionPane.showMessageDialog(null, "Password Must More Than 5 Alphanumeric");
            suPass.clear();
            suCPass.clear();
        } else if (suPass.getText().length() > 99) {
            JOptionPane.showMessageDialog(null, "Password Limit 99 Characters Only");
            suPass.clear();
            suCPass.clear();
        } else if (!(suPass.getText().equals(suCPass.getText()))) {
            JOptionPane.showMessageDialog(null, "Password Not Same");
            suPass.clear();
            suCPass.clear();
        } else {
            tab2.setDisable(false);
            tab.getSelectionModel().selectNext();
            tab1.setDisable(true);
        }
    }

    public void checkBio() {
        if (suRace.getText().isEmpty() || suReli.getText().isEmpty() || suHP.getText().isEmpty() || suHome.getText().isEmpty() || suEmail.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please Fill In All The Blank");
        } else if (suRace.getText().length() > 99 || suReli.getText().length() > 99 || suHP.getText().length() > 15 || suHome.getText().length() > 15 || suEmail.getText().length() > 99) {
            boolean race = suRace.getText().length() > 99;
            boolean reli = suReli.getText().length() > 99;
            boolean email = suEmail.getText().length() > 99;
            boolean hp = suHP.getText().length() > 15;
            boolean home = suHome.getText().length() > 15;
            if (race || reli || email) {
                JOptionPane.showMessageDialog(null, "Limit To 99 Characters Only");
                if (race) {
                    suRace.clear();
                } else if (reli) {
                    suReli.clear();
                } else if (email) {
                    suEmail.clear();
                }
            } else if (hp || home) {
                JOptionPane.showMessageDialog(null, "Limit To 15 Numbers Only");
                if (hp) {
                    suHP.clear();
                } else if (home) {
                    suHome.clear();
                }
            }
        } else if (!isValid(suHP.getText())) {
            JOptionPane.showMessageDialog(null, "Handphone Number Consist Of Number Only");
            suHP.clear();
        } else if (!isValid(suHome.getText())) {
            JOptionPane.showMessageDialog(null, "Home Number Consist Of Number Only");
            suHome.clear();
        } else {
            tab5.setDisable(false);
            tab.getSelectionModel().selectNext();
            tab4.setDisable(true);
        }
    }

    public void checkAdd() throws SQLException, IOException {
        if (suPAdd.getText().isEmpty() || suPPost.getText().isEmpty() || suPCity.getText().isEmpty() || suPSta.getSelectionModel().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please Fill In All The Blank For Primary Address");
        } else if (!isValid(suPPost.getText()) || (Integer.parseInt(suPPost.getText()) > 99999) || (Integer.parseInt(suPPost.getText()) < 10000)) {
            JOptionPane.showMessageDialog(null, "Postcode Consist Number Only And Cannot More Than 99999 Less Than 10000");
            suPPost.clear();
        } else if (suPCity.getText().length() > 99) {
            JOptionPane.showMessageDialog(null, "City Field Limit To 99 Characters Only");
            suPCity.clear();
        } else if (suSAdd.getText().isEmpty() && suSPost.getText().isEmpty() && suSCity.getText().isEmpty() && suSSta.getSelectionModel().isEmpty()) {
            LocalDate date = suDOB.getValue();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String Dated = date.format(formatter);
            db.addStaff(file.toString(), suUser.getText(), suCPass.getText(), suAns.getText(), suName.getText(), suIC.getText(), suGen.getValue(), Dated, suNat.getValue(), suRace.getText(),
                    suReli.getText(), suHP.getText(), suHome.getText(), suEmail.getText(), suPAdd.getText(), suPPost.getText(),
                    suPCity.getText(), suPSta.getValue(), null, null, null, null);
            tab6.setDisable(false);
            tab.getSelectionModel().selectNext();
            tab5.setDisable(true);
        } else if (!(suSAdd.getText().isEmpty() && suSPost.getText().isEmpty() && suSCity.getText().isEmpty() && suSSta.getSelectionModel().isEmpty())) {
            JOptionPane.showMessageDialog(null, "Please Fill In All The Blank For Secondary Address");
        } else if ((Integer.parseInt(suSPost.getText()) > 99999) || (Integer.parseInt(suSPost.getText()) < 10000) || !isValid(suSPost.getText())) {
            JOptionPane.showMessageDialog(null, "Postcode Consist Number Only And Cannot More Than 99999 Less Than 10000");
            suSPost.clear();
        } else if (suSCity.getText().length() > 99) {
            JOptionPane.showMessageDialog(null, "City Field Limit To 99 Characters Only");
            suSCity.clear();
        } else {
            LocalDate date = suDOB.getValue();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String Dated = date.format(formatter);
            db.addStaff(file.toString(), suUser.getText(), suCPass.getText(), suAns.getText(), suName.getText(), suIC.getText(), suGen.getValue(), Dated, suNat.getValue(), suRace.getText(),
                    suReli.getText(), suHP.getText(), suHome.getText(), suEmail.getText(), suPAdd.getText(), suPPost.getText(),
                    suPCity.getText(), suPSta.getValue(), suSAdd.getText(), suSPost.getText(), suSCity.getText(), suSSta.getValue());
            tab6.setDisable(false);
            tab.getSelectionModel().selectNext();
            tab5.setDisable(true);
        }
    }

    public boolean isValid(String num) {
        boolean valid = true;
        int size = num.length();
        int index = 0;
        while (valid && index < size) {
            if (!Character.isDigit(num.charAt(index))) {
                valid = false;
            }
            index++;
        }
        return valid;
    }

    private void bindToTime() {
        Timeline timeline;
        timeline = new Timeline(
                new KeyFrame(Duration.seconds(0), (ActionEvent actionEvent) -> {
                    Calendar time = Calendar.getInstance();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss aa");
                    Time.setText(simpleDateFormat.format(time.getTime()));
                }),
                new KeyFrame(Duration.seconds(1))
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

}
