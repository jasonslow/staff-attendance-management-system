/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bengkel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Windows-8.1
 */
public class SystemSettings implements Initializable {

    DBConnection db;
    GUIStyle gui;
    File file;
    Scanner input;

    @FXML
    private Label sTime;
    @FXML
    private DatePicker pub1;
    @FXML
    private DatePicker pub2;
    @FXML
    private DatePicker pub3;
    @FXML
    private DatePicker pub4;
    @FXML
    private DatePicker pub5;
    @FXML
    private DatePicker pub6;
    @FXML
    private DatePicker pub7;
    @FXML
    private DatePicker pub8;
    @FXML
    private DatePicker pub9;
    @FXML
    private CheckBox che2;
    @FXML
    private CheckBox che1;
    @FXML
    private DatePicker pubop1;
    @FXML
    private DatePicker pubop2;
    @FXML
    private Button phSave;
    @FXML
    private TextField ssStart;
    @FXML
    private TextField ssEnd;

    public SystemSettings() throws SQLException, FileNotFoundException, ClassNotFoundException {
        db = new DBConnection();
        gui = new GUIStyle();
        bindToTime();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        pubop1.setDisable(true);
        pubop2.setDisable(true);
    }

    public void checkpub1() {
        if (che1.isSelected()) {
            int value = JOptionPane.showConfirmDialog(null, "Do you confirm that 2 same holidays in one year ?", "Warning", JOptionPane.YES_NO_OPTION);
            if (value == JOptionPane.YES_OPTION) {
                pubop1.setDisable(false);
                che2.setSelected(false);
                pubop2.setDisable(true);
                pubop2.setValue(null);
            }
            else {
                che1.setSelected(false);
                pubop1.setDisable(true);
                pubop1.setValue(null);
            }
        } else {
                pubop1.setDisable(true);
                pubop1.setValue(null);
        }
    }

    public void checkpub2() {
        if (che2.isSelected()) {
            int value = JOptionPane.showConfirmDialog(null, "Do you confirm that 2 same holidays in one year ?", "Warning", JOptionPane.YES_NO_OPTION);
            if (value == JOptionPane.YES_OPTION) {
                pubop2.setDisable(false);
                che1.setSelected(false);
                pubop1.setDisable(true);
                pubop1.setValue(null);
            }
            else {
                che2.setSelected(false);
                pubop2.setDisable(true);
                pubop2.setValue(null);
            }
        } else {
                pubop2.setDisable(true);
                pubop2.setValue(null);
        }
    }
    
    public void phSave() throws FileNotFoundException, Exception {
        LocalDate date = LocalDate.now();
        int year = date.getYear();
        LocalDate first = date.with(firstDayOfYear());
        LocalDate last = date.with(lastDayOfYear());
        if (ssStart.getText().isEmpty() || ssEnd.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Fill In The Start or End Time");
        } else if (!isValid(ssStart.getText()) && !isValid(ssEnd.getText())) {
            JOptionPane.showMessageDialog(null, "Consist of Numbers Only");
        } else if (Integer.parseInt(ssStart.getText()) >= Integer.parseInt(ssEnd.getText())) {
            JOptionPane.showMessageDialog(null, "End Time Cannot Early Than Start Time And Time Is In 24hours Format");
        } else if (pub1.getValue() == null || pub2.getValue() == null || pub3.getValue() == null || pub4.getValue() == null || pub5.getValue() == null
                || pub6.getValue() == null || pub7.getValue() == null || pub8.getValue() == null || pub9.getValue() == null) {
            JOptionPane.showMessageDialog(null, "Fill In The Blank");
        } else if (pub1.getValue().isAfter(last) || pub2.getValue().isAfter(last) || pub3.getValue().isAfter(last) || pub4.getValue().isAfter(last)
                || pub5.getValue().isAfter(last) || pub6.getValue().isAfter(last) || pub7.getValue().isAfter(last) || pub8.getValue().isAfter(last) || pub9.getValue().isAfter(last)
                || pub1.getValue().isBefore(first) || pub2.getValue().isBefore(first) || pub3.getValue().isBefore(first) || pub4.getValue().isBefore(first)
                || pub5.getValue().isBefore(first) || pub6.getValue().isBefore(first) || pub7.getValue().isBefore(first) || pub8.getValue().isBefore(first) || pub9.getValue().isBefore(first)) {
            JOptionPane.showMessageDialog(null, "For " + date.getYear() + " year public holidays only");
        } else if (che1.isSelected() || che2.isSelected()) {
            if (che1.isSelected()) {
            if (pubop1.getValue() == null && che1.isSelected()) {
                JOptionPane.showMessageDialog(null, "Fill In The Blank");
            }
            else if (pubop1.getValue().isAfter(last) || pubop1.getValue().isBefore(first)) {
                JOptionPane.showMessageDialog(null, "For " + date.getYear() + " year public holidays only");
            } 
            else {
                LocalDate pubhol[] = {pub1.getValue(), pub2.getValue(), pub3.getValue(), pub4.getValue(), pub5.getValue(),
                    pub6.getValue(), pub7.getValue(), pub8.getValue(), pub9.getValue(), pubop1.getValue(),pub2.getValue().plusDays(1),pub5.getValue().plusDays(1), LocalDate.of(year, 1, 1), 
                    LocalDate.of(year, 4, 15), LocalDate.of(year, 5, 1), LocalDate.of(year, 8, 31), LocalDate.of(year, 9, 16), LocalDate.of(year, 12, 25)};
                int total = 0;
                for (int x = 0; x < pubhol.length; x++) {
                    if (!pubhol[x].getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
                        total += 1;
                    }
                }
                String line = year + " " + total + " " + pubhol[0].toString() + " " + pubhol[1].toString() + " " + pubhol[2].toString() + " " + pubhol[3].toString() + " " + pubhol[4].toString() + " " + pubhol[5].toString() + " "
                        + pubhol[6].toString() + " " + pubhol[7].toString() + " " + pubhol[8].toString() + " " + pubhol[9].toString().concat("*") + " " + pubhol[10].toString() + " "
                        + pubhol[11].toString() + " " + pubhol[12].toString() + " " + pubhol[13].toString() + " " + pubhol[14].toString() + " " + pubhol[15].toString() + " "
                        + pubhol[16].toString() + " " + pubhol[17].toString();
                PrintWriter out = new PrintWriter(System.getProperty("user.home")+"/db/system.txt");
                out.println(ssStart.getText());
                out.println(ssEnd.getText());
                out.println(line);
                out.close();
                JOptionPane.showMessageDialog(null, "Save Successfully");
                gui.chgScene("MainMenu.fxml");
            }
            }
            else {
            if (pubop2.getValue() == null && che2.isSelected())
                JOptionPane.showMessageDialog(null, "Fill In The Blank");
            
            else if (pubop2.getValue().isAfter(last) || pubop2.getValue().isBefore(first)) {
                JOptionPane.showMessageDialog(null, "For " + date.getYear() + " year public holidays only");
            }
            else {
                LocalDate pubhol[] = {pub1.getValue(), pub2.getValue(), pub3.getValue(), pub4.getValue(), pub5.getValue(),
                    pub6.getValue(), pub7.getValue(), pub8.getValue(), pub9.getValue(), pubop2.getValue(), pub2.getValue().plusDays(1),pub5.getValue().plusDays(1),LocalDate.of(year, 1, 1), 
                    LocalDate.of(year, 4, 15), LocalDate.of(year, 5, 1), LocalDate.of(year, 8, 31), LocalDate.of(year, 9, 16), LocalDate.of(year, 12, 25)};
                int total = 0;
                for (int x = 0; x < pubhol.length; x++) {
                    if (!pubhol[x].getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
                        total += 1;
                    }
                }
                String line = year + " " + total + " " + pubhol[0].toString() + " " + pubhol[1].toString() + " " + pubhol[2].toString() + " " + pubhol[3].toString() + " " + pubhol[4].toString() + " " + pubhol[5].toString() + " "
                        + pubhol[6].toString() + " " + pubhol[7].toString() + " " + pubhol[8].toString() + " " + pubhol[9].toString().concat("&") + " " + pubhol[10].toString() + " "
                        + pubhol[11].toString() + " " + pubhol[12].toString() + " " + pubhol[13].toString() + " " + pubhol[14].toString() + " " + pubhol[15].toString() + " "
                        + pubhol[16].toString() + " " + pubhol[17].toString();
                PrintWriter out = new PrintWriter(System.getProperty("user.home")+"/db/system.txt");
                out.println(ssStart.getText());
                out.println(ssEnd.getText());
                out.println(line);
                out.close();
                JOptionPane.showMessageDialog(null, "Save Successfully");
                gui.chgScene("MainMenu.fxml");
            }
            }
            
        } else {
            LocalDate pubhol[] = {pub1.getValue(), pub2.getValue(),pub3.getValue(), pub4.getValue(), pub5.getValue(),
                pub6.getValue(), pub7.getValue(), pub8.getValue(), pub9.getValue(),pub2.getValue().plusDays(1),pub5.getValue().plusDays(1), LocalDate.of(year, 1, 1),
                LocalDate.of(year, 4, 15), LocalDate.of(year, 5, 1), LocalDate.of(year, 8, 31), LocalDate.of(year, 9, 16), LocalDate.of(year, 12, 25)};
            int total = 0;
            for (int x = 0; x < pubhol.length; x++) {
                if (!pubhol[x].getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
                    total += 1;
                }
            }
            String line = year + " " + total + " " + pubhol[0].toString() + " " + pubhol[1].toString() + " " + pubhol[2].toString() + " " + pubhol[3].toString() + " " + pubhol[4].toString() + " " + pubhol[5].toString() + " "
                    + pubhol[6].toString() + " " + pubhol[7].toString() + " " + pubhol[8].toString() + " " + pubhol[9].toString() + " " + pubhol[10].toString() + " "
                    + pubhol[11].toString() + " " + pubhol[12].toString() + " " + pubhol[13].toString() + " " + pubhol[14].toString() + " " + pubhol[15].toString() + " " + pubhol[16].toString();
            PrintWriter out = new PrintWriter(System.getProperty("user.home")+"/db/system.txt");
            out.println(ssStart.getText());
            out.println(ssEnd.getText());
            out.println(line);
            out.close();
            JOptionPane.showMessageDialog(null, "Save Successfully");
            gui.chgScene("MainMenu.fxml");
        }

    }

    public boolean isValid(String num) {
        boolean valid = true;
        int size = num.length();
        int index = 0;
        while (valid && index < size) {
            if (!Character.isDigit(num.charAt(index))) {
                valid = false;
            }
            index++;
        }
        return valid;
    }

    private void bindToTime() {
        Timeline timeline;
        timeline = new Timeline(
                new KeyFrame(Duration.seconds(0), (ActionEvent actionEvent) -> {
                    Calendar time = Calendar.getInstance();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss aa");
                    sTime.setText(simpleDateFormat.format(time.getTime()));
                }),
                new KeyFrame(Duration.seconds(1))
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }
}
