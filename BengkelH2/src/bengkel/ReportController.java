/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bengkel;

import com.itextpdf.awt.DefaultFontMapper;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import javax.swing.JOptionPane;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 * FXML Controller class
 *
 * @author Windows-8.1
 */
public class ReportController implements Initializable {

    GUIStyle gui;
    DBConnection db;
    List<User> data;
    ToggleGroup group;
    FileChooser chooser;
    File file;

    @FXML
    private Label grTime;
    @FXML
    private RadioButton grRMon;
    @FXML
    private RadioButton grRYear;
    @FXML
    private ChoiceBox<String> grCMon;
    @FXML
    private ChoiceBox<Integer> grCYear;
    @FXML
    private Button grGen;
    @FXML
    private Hyperlink grBack;

    public ReportController() throws SQLException, ClassNotFoundException {
        gui = new GUIStyle();
        db = new DBConnection();
        group = new ToggleGroup();
        chooser = new FileChooser();
        bindToTime();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        grRMon.setToggleGroup(group);
        grRYear.setToggleGroup(group);
        try {
            grCMon.setItems(db.workdate("GR"));
            if (db.workyear().get(0) != 0) {
                grCYear.setItems(db.workyear());
            }
        } catch (Exception ex) {
        }
        grCMon.setDisable(true);
        grCYear.setDisable(true);
    }

    public void backListener() throws Exception {
        gui.chgScene("AdminMenu.fxml");
    }

    public void rMonListener() {
        grCYear.setDisable(true);
        grCYear.getSelectionModel().select(null);
        grCMon.setDisable(false);
    }

    public void rYearListener() {
        grCMon.setDisable(true);
        grCMon.getSelectionModel().select(null);
        grCYear.setDisable(false);
    }

    public void generateListener() throws SQLException, DocumentException, FileNotFoundException {
        if (!(grRMon.isSelected() || grRYear.isSelected())) {
            JOptionPane.showMessageDialog(null, "Please Select One Of The Option.", "Warning", JOptionPane.ERROR_MESSAGE);
        } else if (grRMon.isSelected()) {
            if (grCMon.getSelectionModel().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Select one of the month.", "Warning", JOptionPane.ERROR_MESSAGE);
            } else {
                String month = grCMon.getValue().substring(3) + "-" + grCMon.getValue().substring(0, 2);
                try {
                    file = new File(chooser.showSaveDialog(null).getPath());
                    String path = file.toString().concat(".pdf");
                    data = db.monReport(month);
                    JFreeChart work = generateWDBarChart(data);
                    JFreeChart ot = generateOTBarChart(data);
                    JFreeChart late = generateLateBarChart(data);
                    JFreeChart leave = generateLeaveBarChart(data);
                    this.generatePDF(path, work, ot, late, leave);
                    JOptionPane.showMessageDialog(null, "PDF Created");
                } catch (Exception e) {
                }
            }
        } else if (grRYear.isSelected()) {
            if (grCYear.getSelectionModel().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Select one of the year.", "Warning", JOptionPane.ERROR_MESSAGE);
            } else {
                String year = grCYear.getValue().toString();
                try {
                    file = new File(chooser.showSaveDialog(null).getPath());
                    String path = file.toString().concat(".pdf");
                    JFreeChart leavepie = ChartFactory.createPieChart("Leave Type In " + year, db.leavepie(year), true, true, false);
                    List<List<String>> leavedata = db.leavetbl(year);
                    JFreeChart leaveline = ChartFactory.createXYLineChart("Number Of Leave Applied In " + year, "Month", "Apply Times", db.xyline(leavedata), PlotOrientation.VERTICAL, true, true, false);
                    JFreeChart latebar = ChartFactory.createBarChart("Late", "Month", "Times", db.latebar(year), PlotOrientation.VERTICAL, false, true, false);
                    List<List<String>> latedata = db.latetbl(year);
                    JFreeChart lateline = ChartFactory.createXYLineChart("Number Of Late In" + year, "Month", "Times", db.xyline(latedata), PlotOrientation.VERTICAL, true, true, false);
                    JFreeChart otbar = ChartFactory.createBarChart("Over Time", "Month", "Hours", db.otbar(year), PlotOrientation.VERTICAL, false, true, false);
                    List<List<String>> otdata = db.ottbl(year);
                    JFreeChart otline = ChartFactory.createXYLineChart("Hours Of Overtime In " + year, "Month", "Hours", db.xyline(otdata), PlotOrientation.VERTICAL, true, true, false);
                    List<List<String>> attdata = db.atttbl(year);
                    JFreeChart attline = ChartFactory.createXYLineChart("Staff Attendance In " + year, "Month", "Days", db.xyline(attdata), PlotOrientation.VERTICAL, true, true, false);
                    List<List<String>> bonusdata = db.bonustbl(year, attdata);
                    this.generateAnnual(path, leavepie, leaveline, latebar, lateline, otbar, otline, attline, leavedata, latedata, otdata, attdata, bonusdata);
                    JOptionPane.showMessageDialog(null, "PDF Created");
                } catch (Exception e) {
                }
            }
        }
    }

    public static JFreeChart generateWDBarChart(List<User> data) {
        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
        for (int x = 0; x < data.size(); x++) {
            dataSet.addValue(data.get(x).getTDate(), "WORK DATE", data.get(x).getName());
        }
        JFreeChart chart = ChartFactory.createBarChart(
                "Working Date", "Staff", "Days",
                dataSet, PlotOrientation.VERTICAL, false, true, false);

        return chart;
    }

    public static JFreeChart generateOTBarChart(List<User> data) {
        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
        for (int x = 0; x < data.size(); x++) {
            dataSet.addValue(data.get(x).getTOt(), "OT", data.get(x).getName());
        }
        JFreeChart chart = ChartFactory.createBarChart("Over Time", "Staff", "Hours", dataSet, PlotOrientation.VERTICAL, false, true, false);
        return chart;
    }

    public static JFreeChart generateLateBarChart(List<User> data) {
        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
        for (int x = 0; x < data.size(); x++) {
            dataSet.addValue(data.get(x).getTLate(), "LATE", data.get(x).getName());
        }
        JFreeChart chart = ChartFactory.createBarChart(
                "Coming Late", "Staff", "Times",
                dataSet, PlotOrientation.VERTICAL, false, true, false);

        return chart;
    }

    public static JFreeChart generateLeaveBarChart(List<User> data) {
        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
        for (int x = 0; x < data.size(); x++) {
            dataSet.addValue(data.get(x).getTLeave(), "Leave", data.get(x).getName());
        }
        JFreeChart chart = ChartFactory.createBarChart(
                "Leave Applied", "Staff", "Times",
                dataSet, PlotOrientation.VERTICAL, false, true, false);

        return chart;
    }

    public void generatePDF(String path, JFreeChart work, JFreeChart ot, JFreeChart late, JFreeChart leave) throws DocumentException, FileNotFoundException {
        PdfWriter writer = null;
        Document document = new Document();
        JFreeChart[] all = {work, ot, late, leave};
        float width = PageSize.A4.getWidth();
        float height = PageSize.A4.getHeight();
        writer = PdfWriter.getInstance(document, new FileOutputStream(path));
        document.open();
        for (int x = 0; x < 4; x++) {
            if (x != 0) {
                document.newPage();
            }
            try {
                PdfContentByte contentByte = writer.getDirectContent();
                PdfTemplate template = contentByte.createTemplate(width, height);
                Graphics2D graphics2d = template.createGraphics(width, height, new DefaultFontMapper());
                Rectangle2D rectangle2d = new Rectangle2D.Double(width * 0.09, height * 0.1, width * 0.8, height * 0.8);
                all[x].draw(graphics2d, rectangle2d);
                graphics2d.dispose();
                contentByte.addTemplate(template, 0, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        document.close();
    }

    public void generateAnnual(String path, JFreeChart leavepie, JFreeChart leaveline, JFreeChart latebar, JFreeChart lateline,
            JFreeChart otbar, JFreeChart otline, JFreeChart attline, List<List<String>> leavedata, List<List<String>> latedata,
            List<List<String>> otdata, List<List<String>> attdata, List<List<String>> bonusdata) throws FileNotFoundException, DocumentException {
        PdfWriter writer = null;
        Document document = new Document();
        JFreeChart[] all = {leavepie, leaveline, latebar, lateline, otbar, otline, attline};
        List<List<List<String>>> datas = new ArrayList<List<List<String>>>();
        datas.add(leavedata);
        datas.add(attdata);
        datas.add(latedata);
        datas.add(bonusdata);
        datas.add(otdata);
        float width = PageSize.A4.getWidth();
        float height = PageSize.A4.getHeight();
        writer = PdfWriter.getInstance(document, new FileOutputStream(path));
        document.open();
        for (int x = 0; x < 6; x = x + 2) {
            try {
                PdfContentByte contentByte = writer.getDirectContent();
                PdfTemplate template = contentByte.createTemplate(width, height);
                Graphics2D graphics2d = template.createGraphics(width, height, new DefaultFontMapper());
                Rectangle2D rectangle2d = new Rectangle2D.Double(width * 0.09, height * 0.1, width * 0.8, height * 0.8);
                all[x].draw(graphics2d, rectangle2d);
                graphics2d.dispose();
                contentByte.addTemplate(template, 0, 0);
                document.newPage();
                contentByte = writer.getDirectContent();
                template = contentByte.createTemplate(width, height);
                graphics2d = template.createGraphics(width, height, new DefaultFontMapper());
                rectangle2d = new Rectangle2D.Double(width * 0.09, height * 0.1, width * 0.8, height * 0.6);
                all[x + 1].draw(graphics2d, rectangle2d);
                graphics2d.dispose();
                contentByte.addTemplate(template, 0, 0);
                PdfPTable table = this.firstcell(width, datas.get(x));
                table.writeSelectedRows(0, -1, (float) (width * 0.1), (float) (height * 0.25), contentByte);
                document.newPage();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        PdfContentByte contentByte = writer.getDirectContent();
        PdfTemplate template = contentByte.createTemplate(width, height);
        Graphics2D graphics2d = template.createGraphics(width, height, new DefaultFontMapper());
        Rectangle2D rectangle2d = new Rectangle2D.Double(width * 0.09, height * 0.1, width * 0.8, height * 0.6);
        all[6].draw(graphics2d, rectangle2d);
        graphics2d.dispose();
        contentByte.addTemplate(template, 0, 0);
        PdfPTable table2 = this.firstcell(width, datas.get(1));
        table2.writeSelectedRows(0, -1, (float) (width * 0.1), (float) (height * 0.25), contentByte);
        document.newPage();
        PdfPTable table = new PdfPTable(3);
        table.addCell(new PdfPCell(new Paragraph("Name")));
        table.addCell(new PdfPCell(new Paragraph("Attendance Rate %")));
        table.addCell(new PdfPCell(new Paragraph("Bonus")));
        table.setHeaderRows(1);
        for (int z = 0; z < bonusdata.size(); z++) {
            table.addCell(new PdfPCell(new Paragraph(bonusdata.get(z).get(0))));
            table.addCell(new PdfPCell(new Paragraph(bonusdata.get(z).get(1))));
            table.addCell(new PdfPCell(new Paragraph(bonusdata.get(z).get(2))));
        }
        table.setTotalWidth((float) (width * 0.8));
        float[] columnWidths = {5f, 4f, 10f};
        table.setWidths(columnWidths);
        table.completeRow();
        table.writeSelectedRows(0, -1, (float) (width * 0.1), (float) (height * 0.9), contentByte);
        document.close();
    }

    public PdfPTable firstcell(float width, List<List<String>> data) throws DocumentException {
        PdfPTable table = new PdfPTable(14);
        table.addCell(new PdfPCell(new Paragraph("Name")));
        for (int x = 1; x <= 12; x++) {
            table.addCell(new PdfPCell(new Paragraph(String.valueOf(x))));
        }
        table.addCell(new PdfPCell(new Paragraph("Total")));
        table.setHeaderRows(1);
        for (int z = 0; z < data.size(); z++) {
            table.addCell(new PdfPCell(new Paragraph(data.get(z).get(0))));
            for (int y = 1; y <= 13; y++) {
                table.addCell(new PdfPCell(new Paragraph(data.get(z).get(y))));
            }
        }
        table.setTotalWidth((float) (width * 0.8));
        float[] columnWidths = {4f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 2f};
        table.setWidths(columnWidths);
        return table;
    }

    private void bindToTime() {
        Timeline timeline;
        timeline = new Timeline(
                new KeyFrame(Duration.seconds(0), (ActionEvent actionEvent) -> {
                    Calendar time = Calendar.getInstance();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss aa");
                    grTime.setText(simpleDateFormat.format(time.getTime()));
                }),
                new KeyFrame(Duration.seconds(1))
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

}
