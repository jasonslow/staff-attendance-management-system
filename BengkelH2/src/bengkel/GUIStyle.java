/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bengkel;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Windows-8.1
 */
public class GUIStyle extends Application {

    private static final Stage stage = new Stage();
    private static final Stage newstage  = new Stage();
    /**
     *
     */
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
            DBConnection db = new DBConnection();
            db.test();
            Parent root = FXMLLoader.load(getClass().getResource("MainMenu.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().addAll(this.getClass().getResource("background.css").toExternalForm());
            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            Parent root = FXMLLoader.load(getClass().getResource("First.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().addAll(this.getClass().getResource("background.css").toExternalForm());
            stage.setScene(scene);
            stage.show();
        }
    }
    
     public void chgScene (String str) throws Exception {
       Parent root = FXMLLoader.load(getClass().getResource(str));
       Scene scene = new Scene(root);
       stage.setScene(scene);
       stage.show();
    }
    
    public void addStage (String str) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource(str));
        Scene scene = new Scene(root);
        try {
            newstage.initModality(Modality.APPLICATION_MODAL);
        } catch (Exception e) {
        }
        scene.getStylesheets().clear();
        newstage.setScene(scene);
        newstage.setResizable(false);
        newstage.show();
    }
    
    public void clearStage () {
        newstage.close();
    }
    
}
