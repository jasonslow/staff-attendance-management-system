/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bengkel;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.util.Callback;
import javafx.util.Duration;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Windows-8.1
 */
public class PendingLeaveController implements Initializable {

    DBConnection db;
    GUIStyle gui;
    ObservableList<User> reqleave;

    @FXML
    private Label piTime;
    @FXML
    private Hyperlink plBack;
    @FXML
    private TableView<User> piTbl;
    @FXML
    private TableColumn<User, Boolean> plCheck;
    @FXML
    private TableColumn<User, String> plName;
    @FXML
    private TableColumn<User, String> plLDate;
    @FXML
    private TableColumn<User, String> plLType;
    @FXML
    private TableColumn<User, String> plDesc;
    @FXML
    private Button plAppAll;
    @FXML
    private Button plApp;
    @FXML
    private Button plDis;

    public PendingLeaveController() throws SQLException, FileNotFoundException, ClassNotFoundException {
        db = new DBConnection();
        gui = new GUIStyle();
        reqleave = db.getPending();
        bindToTime();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        plName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        plLDate.setCellValueFactory(cellData -> cellData.getValue().dateProperty());
        plLType.setCellValueFactory(cellData -> cellData.getValue().typeProperty());
        plDesc.setCellValueFactory(cellData -> cellData.getValue().descProperty());
        plCheck.setCellValueFactory(
                new Callback<CellDataFeatures<User, Boolean>, ObservableValue<Boolean>>() {
                    @Override
                    public ObservableValue<Boolean> call(CellDataFeatures<User, Boolean> param) {
                        return param.getValue().checkProperty();
                    }
                });
        plCheck.setCellFactory(tc -> new CheckBoxTableCell<>());
        piTbl.setItems(reqleave);
    }

    public void backListener() throws Exception {
        if (checkEquals()) {
            int value = JOptionPane.showConfirmDialog(null, "Changes Had Been Made.\nWould you wish to continue ? ", "Please Confirm", JOptionPane.YES_NO_OPTION);
            if (value == JOptionPane.YES_OPTION)
                gui.chgScene("AdminMenu.fxml");
        } else {
            gui.chgScene("AdminMenu.fxml");
        }
    }

    public void clickListener() {
        boolean check;
        int choose = piTbl.getSelectionModel().getSelectedIndex();
        if (choose != -1) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate date = LocalDate.parse(reqleave.get(choose).getDate(), formatter);
            if (reqleave.get(choose).getCheck() == false) {
                check = true;
            } else {
                check = false;
            }
            reqleave.set(choose, new User(check, reqleave.get(choose).getName(),
                    date, reqleave.get(choose).getType(), reqleave.get(choose).getDesc()));
        } else {
            JOptionPane.showMessageDialog(null, "Please One Of The Leave", "Warning", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void appAllListener() throws SQLException, Exception {
        int value = JOptionPane.showConfirmDialog(null, "Are you sure want to approve all?", "Please Confirm", JOptionPane.YES_NO_OPTION);
        if (value == JOptionPane.YES_OPTION) {
            for (int x = 0; x < reqleave.size(); x++) {
                if (reqleave.get(x).getDesc().equals("")) {
                    db.insertLeave(reqleave.get(x).getName(), reqleave.get(x).getDate(), reqleave.get(x).getType(), "NULL");
                } else {
                    db.insertLeave(reqleave.get(x).getName(), reqleave.get(x).getDate(), reqleave.get(x).getType(), reqleave.get(x).getDesc());
                }
            }
            reqleave.clear();
            db.claerLeaved();
            PrintWriter out = new PrintWriter(System.getProperty("user.home")+"/db/leave.txt");
            out.close();
            JOptionPane.showMessageDialog(null, "All Leave Had Been Approved");
            gui.chgScene("AdminMenu.fxml");
        }
    }

    public void appListener() throws SQLException, Exception {
        int value = JOptionPane.showConfirmDialog(null, "Are you sure want to approve ?", "Please Confirm", JOptionPane.YES_NO_OPTION);
        if (value == JOptionPane.YES_OPTION) {
            if (checkEquals()) {
                for (int x = (reqleave.size()-1); 0 <= x; x--) {
                    if (reqleave.get(x).getCheck() == true) {
                        if (reqleave.get(x).getDesc().equals("")) {
                            db.insertLeave(reqleave.get(x).getName(), reqleave.get(x).getDate(), reqleave.get(x).getType(), "NULL");
                        } else {
                            db.insertLeave(reqleave.get(x).getName(), reqleave.get(x).getDate(), reqleave.get(x).getType(), reqleave.get(x).getDesc());
                        }
                        reqleave.remove(x);
                    }
                }
                PrintWriter out = new PrintWriter(System.getProperty("user.home")+"/db/leave.txt");
                for (int y = 0; y < reqleave.size(); y++) {
                    out.println(reqleave.get(y).getName()); 
                    out.println(reqleave.get(y).getDate());
                    out.println(reqleave.get(y).getType());
                    if(reqleave.get(y).getDesc().equals(""))
                        out.println("NULL");
                    else
                        out.println(reqleave.get(y).getDesc());
                }
                out.close();
                JOptionPane.showMessageDialog(null, "Leave Had Been Approved");
                if (reqleave.size()==0) {
                    db.claerLeaved();
                    gui.chgScene("AdminMenu.fxml");
                }
            } else {
                JOptionPane.showMessageDialog(null, "No Changes");
            }
        }
    }

    public void disappListener() throws Exception {
        int value = JOptionPane.showConfirmDialog(null, "Are you sure want to disapprove ?", "Please Confirm", JOptionPane.YES_NO_OPTION);
        if (value == JOptionPane.YES_OPTION) {
            if (checkEquals()) {
                for (int x = (reqleave.size()-1); 0<=x; x--) {
                    if (reqleave.get(x).getCheck() == true) {
                         reqleave.remove(x);
                    }
                }
                PrintWriter out = new PrintWriter(System.getProperty("user.home")+"/db/leave.txt");
                for (int y = 0; y < reqleave.size(); y++) {
                    out.println(reqleave.get(y).getName()); 
                    out.println(reqleave.get(y).getDate());
                    out.println(reqleave.get(y).getType());
                    if(reqleave.get(y).getDesc().equals(""))
                        out.println("NULL");
                    else
                        out.println(reqleave.get(y).getDesc());
                }
                out.close();
                JOptionPane.showMessageDialog(null, "Leave Had Been Disapproved");
                if (reqleave.size()==0) {
                    db.claerLeaved();
                    gui.chgScene("AdminMenu.fxml");
                }
            } else {
                JOptionPane.showMessageDialog(null, "No Changes");
            }
        }
    }
    
    public boolean checkEquals() throws FileNotFoundException {
        boolean check = false;
        if (reqleave.size()!=0) {
            int index = 0;
            int size = reqleave.size();
            while(index<size) {
                if (reqleave.get(index).getCheck() != db.getPending().get(index).getCheck()){
                    check = true;
                    index = size;
                }
                index++;
            }
        }
            return check;
    }

    private void bindToTime() {
        Timeline timeline;
        timeline = new Timeline(
                new KeyFrame(Duration.seconds(0), (ActionEvent actionEvent) -> {
                    Calendar time = Calendar.getInstance();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss aa");
                    piTime.setText(simpleDateFormat.format(time.getTime()));
                }),
                new KeyFrame(Duration.seconds(1))
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

}
