/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bengkel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Windows-8.1
 */
public class PersonalInfoController implements Initializable {

    GUIStyle gui;
    DBConnection db;
    ToggleGroup group;
    String path;
    String[] pi;
    Image image;
    File file;
    File newfile = null;
    ObservableList<User> data;
    ObservableList<String> gender = FXCollections.observableArrayList("M", "F");
    ObservableList<String> nation = FXCollections.observableArrayList("Malaysian", "Non-Malaysian");
    ObservableList<String> state = FXCollections.observableArrayList("WPKL", "Selangor", "Melaka", "Perak", "Negeri Sembilan",
            "Pahang", "Johor", "Kedah", "Terengganu", "Pulau Pinang",
            "Perlis", "Kelantan", "Sabah", "Sarawak", "Labuan", "Putrajaya");

    @FXML
    private TextField piName;
    @FXML
    private TextField piIC;
    @FXML
    private TextField piRace;
    @FXML
    private TextField piReli;
    @FXML
    private TextField piHP;
    @FXML
    private TextField piHome;
    @FXML
    private TextField piEmail;
    @FXML
    private ChoiceBox<String> piGender;
    @FXML
    private DatePicker piDOB;
    @FXML
    private ChoiceBox<String> piNation;
    @FXML
    private TextArea piPriAdd;
    @FXML
    private TextField piPriPost;
    @FXML
    private TextField piPriCity;
    @FXML
    private ChoiceBox<String> piPriState;
    @FXML
    private TextArea piSecAdd;
    @FXML
    private TextField piSecPost;
    @FXML
    private TextField piSecCity;
    @FXML
    private ChoiceBox<String> piSecState;
    @FXML
    private ChoiceBox<String> piMonYear;
    @FXML
    private Button piUP;
    @FXML
    private Button piSave;
    @FXML
    private Button piSearch;
    @FXML
    private RadioButton pirAtt;
    @FXML
    private RadioButton piLeave;
    @FXML
    private TableView<User> piTbl;
    @FXML
    private TableColumn<User, String> piWorkDate;
    @FXML
    private TableColumn<User, String> piTIn;
    @FXML
    private TableColumn<User, String> piTOut;
    @FXML
    private TableColumn<User, String> piOT;
    @FXML
    private TableColumn<User, String> piCL;
    @FXML
    private Button piUserSave;
    @FXML
    private Button piUserCancel;
    @FXML
    private PasswordField piOldPass;
    @FXML
    private PasswordField piNewPass;
    @FXML
    private PasswordField piRePass;
    @FXML
    private Hyperlink piBack;
    @FXML
    private Label Time;
    @FXML
    private ImageView piImage;

    public PersonalInfoController() throws SQLException, ClassNotFoundException {
        db = new DBConnection();
        gui = new GUIStyle();
        pi = db.personalInfo();
        group = new ToggleGroup();
        file = new File(System.getProperty("user.home") + "/db/" + pi[1]);
        image = new Image(file.toURI().toString());
        bindToTime();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        piImage.setImage(image);
        piGender.setItems(gender);
        piNation.setItems(nation);
        piPriState.setItems(state);
        piName.setText(pi[2]);
        piIC.setText(pi[3]);
        piGender.getSelectionModel().select(pi[4]);
        piDOB.setValue(LocalDate.parse(pi[5]));
        piNation.getSelectionModel().select(pi[6]);
        piRace.setText(pi[7]);
        piReli.setText(pi[8]);
        piHP.setText(("0" + pi[9]));
        piHome.setText(("0" + pi[10]));
        piEmail.setText(pi[11]);
        piPriAdd.setText(pi[12]);
        piPriPost.setText(pi[13]);
        piPriCity.setText(pi[14]);
        piPriState.getSelectionModel().select(pi[15]);
        piSecState.setItems(state);
        if (!pi[16].equalsIgnoreCase("NULL")) {
            piSecAdd.setText(pi[16]);
            piSecPost.setText(pi[17]);
            piSecCity.setText(pi[18]);
            piSecState.getSelectionModel().select(pi[19]);
        }
        piWorkDate.setCellValueFactory(cellData -> cellData.getValue().dateProperty());
        piTIn.setCellValueFactory(cellData -> cellData.getValue().timeInProperty());
        piTOut.setCellValueFactory(cellData -> cellData.getValue().timeOutProperty());
        piOT.setCellValueFactory(cellData -> cellData.getValue().OTProperty());
        piCL.setCellValueFactory(cellData -> cellData.getValue().LateProperty());
        piLeave.setToggleGroup(group);
        pirAtt.setToggleGroup(group);
        piMonYear.setDisable(true);
        try {
            piMonYear.setItems(db.workdate("PI"));
        } catch (Exception ex) {
        }
    }

    public void userSaveListener() throws SQLException {
        if (piOldPass.getText().isEmpty() || piNewPass.getText().isEmpty() || piRePass.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please Fill In All Blank", "Warning", JOptionPane.ERROR_MESSAGE);
            userCancelListener();
        } else if (!piOldPass.getText().equals(db.getPass())) {
            JOptionPane.showMessageDialog(null, "Old Password Not Same", "Warning", JOptionPane.ERROR_MESSAGE);
            userCancelListener();
        } else if (piNewPass.getText().length() < 5 || piNewPass.getText().length() > 99) {
            JOptionPane.showMessageDialog(null, "New Password And Retype Password Not Same", "Warning", JOptionPane.ERROR_MESSAGE);
            piNewPass.clear();
            piRePass.clear();
        } else if (!piNewPass.getText().equals(piRePass.getText())) {
            JOptionPane.showMessageDialog(null, "New Password And Retype Password Not Same", "Warning", JOptionPane.ERROR_MESSAGE);
            piNewPass.clear();
            piRePass.clear();
        } else {
            db.upPass(piRePass.getText());
            JOptionPane.showMessageDialog(null, "User Password Renew");
            userCancelListener();
        }
    }

    public void userCancelListener() {
        piOldPass.clear();
        piNewPass.clear();
        piRePass.clear();
    }

    public void searchListener() throws SQLException, FileNotFoundException {
        if (pirAtt.isSelected()) {
            try {
                data = db.piReport(piMonYear.getValue());
                int total = 0;
                int late = 0;
                for (int x = 0; x < data.size(); x++) {
                    total += Integer.parseInt(data.get(x).getOT());
                    if (data.get(x).getLate().equals("YES")) {
                        late++;
                    }
                }
                data.add(new User("", "", "", String.valueOf(total), String.valueOf(late)));
                piWorkDate.setCellValueFactory(cellData -> cellData.getValue().dateProperty());
                piTbl.setItems(data);
            } catch (NullPointerException e) {
                JOptionPane.showMessageDialog(null, "Please Select One of the Month");
            }
        } else {
            data = db.piLeave();
            piWorkDate.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
            int index = 0;
            while (data.size() > index) {
                if (data.get(index).getTimeOut().equals("NULL")) {
                    data.set(index, new User(data.get(index).getName(), data.get(index).getTimeIn(), ""));
                }
                index++;
            }
            piTbl.setItems(data);
        }
    }

    public void backListener() throws Exception {
        String[] npi = db.personalInfo();
        if (pi[16].equalsIgnoreCase("NULL")) {
            if (!(newfile == null && piName.getText().equals(npi[2]) && piIC.getText().equals(npi[3]) && piGender.getValue().equals(npi[4]) && piDOB.getValue().equals(LocalDate.parse(npi[5]))
                    && piNation.getValue().equals(npi[6]) && piRace.getText().equals(npi[7]) && piReli.getText().equals(npi[8]) && piHP.getText().equals("0" + npi[9])
                    && piHome.getText().equals("0" + npi[10]) && piEmail.getText().equals(npi[11]) && piPriAdd.getText().equals(npi[12]) && piPriPost.getText().equals(npi[13])
                    && piPriCity.getText().equals(npi[14]) && piPriState.getValue().equals(npi[15]) && piSecAdd.getText().isEmpty() && piSecPost.getText().isEmpty()
                    && piSecCity.getText().isEmpty() && piSecState.getSelectionModel().isEmpty())) {
                back();
            } else {
                if (db.getVerify().equals("user")) {
                    gui.chgScene("MainMenu.fxml");
                } else {
                    gui.chgScene("AdminMenu.fxml");
                }
            }
        } else if (!pi[16].equalsIgnoreCase("NULL")) {
            if (!(newfile == null && piName.getText().equals(npi[2]) && piIC.getText().equals(npi[3]) && piGender.getValue().equals(npi[4]) && piDOB.getValue().equals(LocalDate.parse(npi[5]))
                    && piNation.getValue().equals(npi[6]) && piRace.getText().equals(npi[7]) && piReli.getText().equals(npi[8]) && piHP.getText().equals("0" + npi[9])
                    && piHome.getText().equals("0" + npi[10]) && piEmail.getText().equals(npi[11]) && piPriAdd.getText().equals(npi[12]) && piPriPost.getText().equals(npi[13])
                    && piPriCity.getText().equals(npi[14]) && piPriState.getValue().equals(npi[15]) && piSecAdd.getText().equals(npi[16]) && piSecPost.getText().equals(npi[17])
                    && piSecCity.getText().equals(npi[18]) && piSecState.getValue().equals(npi[19]))) {
                back();
            } else {
                if (db.getVerify().equals("user")) {
                    gui.chgScene("MainMenu.fxml");
                } else {
                    gui.chgScene("AdminMenu.fxml");
                }
            }
        }
    }

    public void saveListener() throws SQLException, IOException {
        check();
    }

    public void uploadListener() {
        FileChooser chooser = new FileChooser();
        try {
            newfile = new File(chooser.showOpenDialog(null).getPath());
            image = new Image(newfile.toURI().toString());
            if (image.isError()) {
                JOptionPane.showMessageDialog(null, "Wrong Picture Format.");
                newfile = null;
                image = new Image(file.toURI().toString());
                piImage.setImage(image);
            } else {
                piImage.setImage(image);
            }
        } catch (NullPointerException e) {
        }
    }

    public void radioLeave() {
        piMonYear.setDisable(true);
        piMonYear.getSelectionModel().select(null);
        try {
            data.clear();
        } catch (Exception e) {
        }
        piWorkDate.setText("Leave Date");
        piTIn.setText("Type");
        piTOut.setText("Description");
        piOT.setText("");
        piCL.setText("");
        piWorkDate.setPrefWidth(200);
        piTIn.setPrefWidth(200);
        piTOut.setPrefWidth(200);
        piOT.setPrefWidth(0);
        piCL.setPrefWidth(0);
    }

    public void radioMonYear() {
        piMonYear.setDisable(false);
        try {
            data.clear();
        } catch (Exception e) {
        }
        piWorkDate.setText("Work Date");
        piTIn.setText("Time In");
        piTOut.setText("Time Out");
        piOT.setText("OT");
        piCL.setText("Late");
        piWorkDate.setPrefWidth(120);
        piTIn.setPrefWidth(120);
        piTOut.setPrefWidth(120);
        piOT.setPrefWidth(120);
        piCL.setPrefWidth(120);
    }

    public boolean check() throws SQLException, IOException {
        boolean check = false;
        if (piName.getText().isEmpty() || piIC.getText().isEmpty() || piGender.getSelectionModel().isEmpty() || piDOB.getValue() == null || piNation.getSelectionModel().isEmpty()
                || piRace.getText().isEmpty() || piReli.getText().isEmpty() || piHP.getText().isEmpty() || piHome.getText().isEmpty() || piEmail.getText().isEmpty()
                || piPriAdd.getText().isEmpty() || piPriPost.getText().isEmpty() || piPriCity.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please Fill In The Blank");
        } else if (image.isError()) {
            JOptionPane.showMessageDialog(null, "Wrong Image Format.\nPlease Rechoose A Valid Image");
        } else if (piName.getText().length() > 99 || piIC.getText().length() > 99 || piRace.getText().length() > 99 || piReli.getText().length() > 99
                || piEmail.getText().length() > 99 || piPriCity.getText().length() > 99) {
            boolean[] valid = {piName.getText().length() > 99, piIC.getText().length() > 99, piRace.getText().length() > 99, piReli.getText().length() > 99,
                piEmail.getText().length() > 99, piPriCity.getText().length() > 99};
            JOptionPane.showMessageDialog(null, "Limit 99 Characters Only");
            if (valid[0]) {
                piName.clear();
            } else if (valid[1]) {
                piIC.clear();
            } else if (valid[2]) {
                piRace.clear();
            } else if (valid[3]) {
                piReli.clear();
            } else if (valid[4]) {
                piEmail.clear();
            } else if (valid[5]) {
                piPriCity.clear();
            }
        } else if (!db.CheckNameIC(null, piIC.getText())) {
            JOptionPane.showMessageDialog(null, "Same IC or Passport Number Found");
            piIC.setText(pi[3]);
        } else if (!(isValid(piPriPost.getText())) || !(isValid(piHP.getText())) || !(isValid(piHome.getText()))) {
            JOptionPane.showMessageDialog(null, "Postcode, Handphone and Home Number Consist Number Only");
        } else if ((Integer.parseInt(piPriPost.getText()) > 99999) || (Integer.parseInt(piPriPost.getText()) < 10000)) {
            JOptionPane.showMessageDialog(null, "Postcode Range Within 10000 to 99999");
        } else if (pi[16].equalsIgnoreCase("NULL")) {
            if ((piSecAdd.getText().isEmpty() || piSecPost.getText().isEmpty() || piSecCity.getText().isEmpty() || piSecState.getSelectionModel().isEmpty())
                    && !(piSecAdd.getText().isEmpty() && piSecPost.getText().isEmpty() && piSecCity.getText().isEmpty() && piSecState.getSelectionModel().isEmpty())) {
                JOptionPane.showMessageDialog(null, "Please Fill In All The Blanks For Secondary Address");
            } else if (!(piSecAdd.getText().isEmpty() || piSecPost.getText().isEmpty() || piSecCity.getText().isEmpty() || piSecState.getValue().isEmpty())) {
                if (!(isValid(piSecPost.getText()))) {
                    JOptionPane.showMessageDialog(null, "Postcode Consist Number Only.");
                } else if ((Integer.parseInt(piSecPost.getText()) > 99999) || (Integer.parseInt(piSecPost.getText()) < 10000)) {
                    JOptionPane.showMessageDialog(null, "Postcode Range Within 10000 to 99999");
                } else {
                    if (newfile == null) {
                        path = file.toString();
                    } else {
                        path = newfile.toString();
                    }
                    if (!pi[1].equalsIgnoreCase("default.png")) {
                        File files = new File(System.getProperty("user.home") + "/db/" + pi[1]);
                        files.delete();
                    }
                    db.upStaff(path, piName.getText(), piIC.getText(), piGender.getValue(), piDOB.getValue().toString(), piNation.getValue(), piRace.getText(),
                            piReli.getText(), piHP.getText(), piHome.getText(), piEmail.getText(), piPriAdd.getText(), piPriPost.getText(),
                            piPriCity.getText(), piPriState.getValue(), piSecAdd.getText(), piSecPost.getText(), piSecCity.getText(), piSecState.getValue());
                    check = true;
                    newfile = null;
                    JOptionPane.showMessageDialog(null, "Changes had been saved");
                }
            } else if (!(newfile == null && piName.getText().equals(pi[2]) && piIC.getText().equals(pi[3]) && piGender.getValue().equals(pi[4]) && piDOB.getValue().equals(LocalDate.parse(pi[5]))
                    && piNation.getValue().equals(pi[6]) && piRace.getText().equals(pi[7]) && piReli.getText().equals(pi[8]) && piHP.getText().equals("0" + pi[9])
                    && piHome.getText().equals("0" + pi[10]) && piEmail.getText().equals(pi[11]) && piPriAdd.getText().equals(pi[12]) && piPriPost.getText().equals(pi[13])
                    && piPriCity.getText().equals(pi[14]) && piPriState.getValue().equals(pi[15]))) {
                if (newfile == null) {
                    path = file.toString();
                } else {
                    path = newfile.toString();
                }
                if (!pi[1].equalsIgnoreCase("default.png")) {
                    File files = new File(System.getProperty("user.home") + "/db/" + pi[1]);
                    files.delete();
                }
                db.upStaff(path, piName.getText(), piIC.getText(), piGender.getValue(), piDOB.getValue().toString(), piNation.getValue(), piRace.getText(),
                        piReli.getText(), piHP.getText(), piHome.getText(), piEmail.getText(), piPriAdd.getText(), piPriPost.getText(),
                        piPriCity.getText(), piPriState.getValue(), null, null, null, null);
                check = true;
                newfile = null;
                JOptionPane.showMessageDialog(null, "Changes had been saved.");
            } else {
                JOptionPane.showMessageDialog(null, "No Changes");
            }
        } else {
            if (piSecAdd.getText().isEmpty() || piSecPost.getText().isEmpty() || piSecCity.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Please Fill In All The Blanks For Secondary Address");
            } else if (((Integer.parseInt(piSecPost.getText()) > 99999) || (Integer.parseInt(piSecPost.getText()) < 10000)) || !(isValid(piSecPost.getText()))) {
                JOptionPane.showMessageDialog(null, "Postcode Consist Number Only Range Within 10000 to 99999");
            } else if (!(newfile == null && piName.getText().equals(pi[2]) && piIC.getText().equals(pi[3]) && piGender.getValue().equals(pi[4]) && piDOB.getValue().equals(LocalDate.parse(pi[5]))
                    && piNation.getValue().equals(pi[6]) && piRace.getText().equals(pi[7]) && piReli.getText().equals(pi[8]) && piHP.getText().equals("0" + pi[9])
                    && piHome.getText().equals("0" + pi[10]) && piEmail.getText().equals(pi[11]) && piPriAdd.getText().equals(pi[12]) && piPriPost.getText().equals(pi[13])
                    && piPriCity.getText().equals(pi[14]) && piPriState.getValue().equals(pi[15]) && piSecAdd.getText().equals(pi[16]) && piSecPost.getText().equals(pi[17])
                    && piSecCity.getText().equals(pi[18]) && piSecState.getValue().equals(pi[19]))) {
                if (newfile == null) {
                    path = file.toString();
                } else {
                    path = newfile.toString();
                }
                if (!pi[1].equalsIgnoreCase("default.png")) {
                    File files = new File(System.getProperty("user.home") + "/db/" + pi[1]);
                    files.delete();
                }
                db.upStaff(path, piName.getText(), piIC.getText(), piGender.getValue(), piDOB.getValue().toString(), piNation.getValue(), piRace.getText(),
                        piReli.getText(), piHP.getText(), piHome.getText(), piEmail.getText(), piPriAdd.getText(), piPriPost.getText(),
                        piPriCity.getText(), piPriState.getValue(), piSecAdd.getText(), piSecPost.getText(), piSecCity.getText(), piSecState.getValue());
                check = true;
                newfile = null;
                JOptionPane.showMessageDialog(null, "Changes Had Been Made");
            } else {
                JOptionPane.showMessageDialog(null, "No Changes");
            }
        }
        return check;
    }

    public void back() throws Exception {
        boolean valid = false;
        int value = JOptionPane.showConfirmDialog(null, "There are changes in your biodata.\nDid you want to save this changes ?");
        if (value == JOptionPane.YES_OPTION) {
            valid = check();
            if (valid == true) {
                if (db.getVerify().equals("user")) {
                    gui.chgScene("MainMenu.fxml");
                } else {
                    gui.chgScene("AdminMenu.fxml");
                }
            }
        } else if (value == JOptionPane.NO_OPTION) {
            JOptionPane.showMessageDialog(null, "Changes not saved.\n Logged Out");
            if (db.getVerify().equals("user")) {
                gui.chgScene("MainMenu.fxml");
            } else {
                gui.chgScene("AdminMenu.fxml");
            }
        }
    }

    public boolean isValid(String num) {
        boolean valid = true;
        int size = num.length();
        int index = 0;
        while (valid && index < size) {
            if (!Character.isDigit(num.charAt(index))) {
                valid = false;
            }
            index++;
        }
        return valid;
    }

    private void bindToTime() {
        Timeline timeline;
        timeline = new Timeline(
                new KeyFrame(Duration.seconds(0), (ActionEvent actionEvent) -> {
                    Calendar time = Calendar.getInstance();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss aa");
                    Time.setText(simpleDateFormat.format(time.getTime()));
                }),
                new KeyFrame(Duration.seconds(1))
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }
}
