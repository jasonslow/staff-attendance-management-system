/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bengkel;

import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.util.Duration;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Windows-8.1
 */
public class AdminMenuController implements Initializable {
    
    DBConnection db;
    GUIStyle gui;
    
    @FXML
    private Label Time;
    
    @FXML
    private Button amPenReq;

    @FXML
    private Button amSet;

    @FXML
    private Button amPerInfo;

    @FXML
    private Button amReport;

    @FXML
    private Hyperlink amBack;
    
    public AdminMenuController() throws SQLException, ClassNotFoundException {
        db = new DBConnection();
        gui = new GUIStyle();
        bindToTime();
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void PerInfoListener() throws Exception {
        gui.chgScene("PersonalInfo.fxml");
    }
    
    public void BackListener() throws Exception {
        gui.chgScene("MainMenu.fxml");
    }
    
    public void ReportListener() throws Exception {
        gui.chgScene("Report.fxml");
    }
    
    public void PendingListener() throws Exception {
        try {
        db.getPending().isEmpty();
        gui.chgScene("PendingLeave.fxml");
        }
        catch(Exception e) {
            JOptionPane.showMessageDialog(null, "No Leave Apply");
        }
    }
    
    public void SettingListener() throws Exception {
        gui.chgScene("Settings.fxml");
    }
    
    private void bindToTime() {
        Timeline timeline;
        timeline = new Timeline(
                new KeyFrame(Duration.seconds(0), (ActionEvent actionEvent) -> {
                    Calendar time = Calendar.getInstance();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss aa");
                    Time.setText(simpleDateFormat.format(time.getTime()));
                }),
                new KeyFrame(Duration.seconds(1))
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }
}
