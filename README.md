# Staff Attendance Management System

# Introduction

This system provides ease to use as it has a user-friendly interface and reliable staff attendance record and report. It also allows staff to punch-in and punch-out and apply leave application. Moreover, the system will store staff personal details information and monitor staff working time. The system will calculate staff working day, overtime, coming late and amount of leave. Based on the calculation, system can generate various report such as staffs attendance report, monthly attendance report and attendance analysis. The system will also calculate staff bonuses based on staff attendance. This system developed is to overcome problem from the tradition punch time clock system and provide an effective way to monitor staff attendance.

# Usage

Open Application:

1) Choose the application from folder "Application" and based on Windows System Type.

2) Register admin.

To View Database Data:

1) Download H2 Windows Installer from "http://www.h2database.com/html/download.html"

2) Open C:\Program Files (x86)\H2\bin\h2.bat

3) Make sure follow as below


   Driver Class:	org.h2.Driver
   
   JDBC URL:		jdbc:h2:~/db/bengkel
   
   User Name:		root
   
   Password:		toor	
   


4) Connect it and type SQL to view database data.
